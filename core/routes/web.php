<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use App\Models\User;
// Route::get('/mysql', function(){
//     $pdo = DB::connection('mysql');
//     dd($pdo);
// });
Route::get('/cek_url', function(){
    dd(URL::to('/'));
});
Route::get('/cek_ip', function(){
    $realIP = file_get_contents("http://ipecho.net/plain");
    dd($realIP);
});
Route::get('/cek_bit', function(){
    dd(PHP_INT_MAX);
});
Route::get('/clear', function(){
    \Illuminate\Support\Facades\Artisan::call('optimize:clear');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
});
Route::get('/key', function(){
    \Illuminate\Support\Facades\Artisan::call('key:generate');
});
Route::get('/voldemort/up', function(){
    \Illuminate\Support\Facades\Artisan::call('up');
    echo "Website on Live Production";
});
Route::get('/voldemort/down', function(){
    \Illuminate\Support\Facades\Artisan::call('down --secret="harrypotter"');
    echo "Website on Maintenance";
});

Route::get('/generateUniqueCode/{no_bro}', 'Auth\RegisterController@generateUniqueCode');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/


// Route::get('/cron', 'CronController@cron')->name('bv.matching.cron');
// Route::get('/cron30bro', 'CronController@cron30bro')->name('bv.matching.cron30bro');

Route::namespace('Gateway')->prefix('ipn')->name('ipn.')->group(function () {
    Route::post('paypal', 'paypal\ProcessController@ipn')->name('paypal');
    Route::get('paypal_sdk', 'paypal_sdk\ProcessController@ipn')->name('paypal_sdk');
    Route::post('perfect_money', 'perfect_money\ProcessController@ipn')->name('perfect_money');
    Route::post('stripe', 'stripe\ProcessController@ipn')->name('stripe');
    Route::post('stripe_js', 'stripe_js\ProcessController@ipn')->name('stripe_js');
    Route::post('stripe_v3', 'stripe_v3\ProcessController@ipn')->name('stripe_v3');
    Route::post('skrill', 'skrill\ProcessController@ipn')->name('skrill');
    Route::post('paytm', 'paytm\ProcessController@ipn')->name('paytm');
    Route::post('payeer', 'payeer\ProcessController@ipn')->name('payeer');
    Route::post('paystack', 'paystack\ProcessController@ipn')->name('paystack');
    Route::post('voguepay', 'voguepay\ProcessController@ipn')->name('voguepay');
    Route::get('flutterwave/{trx}/{type}', 'flutterwave\ProcessController@ipn')->name('flutterwave');
    Route::post('razorpay', 'razorpay\ProcessController@ipn')->name('razorpay');
    Route::post('instamojo', 'instamojo\ProcessController@ipn')->name('instamojo');
    Route::get('blockchain', 'blockchain\ProcessController@ipn')->name('blockchain');
    Route::get('blockio', 'blockio\ProcessController@ipn')->name('blockio');
    Route::post('coinpayments', 'coinpayments\ProcessController@ipn')->name('coinpayments');
    Route::post('coinpayments_fiat', 'coinpayments_fiat\ProcessController@ipn')->name('coinpayments_fiat');
    Route::post('coingate', 'coingate\ProcessController@ipn')->name('coingate');
    Route::post('coinbase_commerce', 'coinbase_commerce\ProcessController@ipn')->name('coinbase_commerce');
    Route::get('mollie', 'mollie\ProcessController@ipn')->name('mollie');
    Route::post('cashmaal', 'cashmaal\ProcessController@ipn')->name('cashmaal');
});

// User Support Ticket
Route::prefix('ticket')->group(function () {
    Route::get('/', 'TicketController@supportTicket')->name('ticket');
    Route::get('/new', 'TicketController@openSupportTicket')->name('ticket.open');
    Route::post('/create', 'TicketController@storeSupportTicket')->name('ticket.store');
    Route::get('/view/{ticket}', 'TicketController@viewTicket')->name('ticket.view');
    Route::post('/reply/{ticket}', 'TicketController@replyTicket')->name('ticket.reply');
    Route::get('/download/{ticket}', 'TicketController@ticketDownload')->name('ticket.download');
});


/*
|--------------------------------------------------------------------------
| Start Admin Area
|--------------------------------------------------------------------------
*/
Route::get('/cron', 'CronController@cron')->name('bv.matching.cron');
Route::get('/cron_flush_out', 'CronController@cronFlushOut')->name('bv.matching.cron.flush.out');
Route::get('/balance_on_hold', 'CronController@balanceOnHold')->name('bv.matching.boh');
Route::get('/uplevel', 'CronController@uplevel')->name('bv.matching.up');
Route::get('/uplevelDP', 'CronController@uplevelDP')->name('bv.matching.upDP');
Route::get('/cronsave', 'CronController@cronsave')->name('bv.matching.cronsave');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::get('/', 'LoginController@showLoginForm')->name('login');
        Route::post('/', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');
        // Admin Password Reset
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
        Route::post('password/reset', 'ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/verify-code', 'ForgotPasswordController@verifyCode')->name('password.verify-code');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.change-link');
        Route::post('password/reset/change', 'ResetPasswordController@reset')->name('password.change');
    });

    Route::middleware('admin')->group(function () {
        Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
        Route::get('dashboards', 'AdminController@dashboards')->name('dashboards');
        Route::get('profile', 'AdminController@profile')->name('profile');
        Route::post('profile', 'AdminController@profileUpdate')->name('profile.update');
        Route::get('password', 'AdminController@password')->name('password');
        Route::post('password', 'AdminController@passwordUpdate')->name('password.update');

        Route::get('activity', 'AdminlogController@index')->name('activity');
        
        // Route::get('alamat_user', function(){
        //     $user = User::select('*')->where('plan_id','!=',0)->where('email', 'not like', "%+%")->get();
        //     foreach ($user as $element) {
        //         // $r = explode(':',$element->address->city);
        //         // $result[] = $r[5];
        //         $result[] = $element->address->city;
        //     }
        //     dd($result);
        // });

        //Manage Survey
        Route::get('survey/category/all', 'SurveyController@allCategory')->name('survey.category.all');
        Route::post('survey/category/add', 'SurveyController@addCategory')->name('survey.category.add');
        Route::post('survey/category/update', 'SurveyController@updateCategory')->name('survey.category.update');

        Route::get('survey/report', 'SurveyController@report')->name('survey.report');
        Route::get('survey/report/{id}', 'SurveyController@reportQuestion')->name('survey.report.question');
        Route::get('survey/report/download/{id}', 'SurveyController@reportDownload')->name('survey.report.download');

        Route::get('survey/all', 'SurveyController@allSurvey')->name('survey.all');
        Route::get('survey/new', 'SurveyController@newSurvey')->name('survey.new');
        Route::post('survey/add', 'SurveyController@addSurvey')->name('survey.add');
        Route::get('survey/edit/{id}', 'SurveyController@editSurvey')->name('survey.edit');
        Route::post('survey/update', 'SurveyController@updateSurvey')->name('survey.update');

        Route::get('survey/all/question/{id}', 'SurveyController@allQuestion')->name('survey.all.question');
        Route::get('survey/new/question/{id}', 'SurveyController@newQuestion')->name('survey.new.question');
        Route::post('survey/question/add', 'SurveyController@addQuestion')->name('survey.add.question');
        Route::get('survey/question/edit/{question_id}/{survey_id}', 'SurveyController@editQuestion')->name('survey.question.edit');
        Route::post('survey/question/update', 'SurveyController@updateQuestion')->name('survey.question.update');

        Route::get('notification/read/{id}','AdminController@notificationRead')->name('notification.read');
        Route::get('notifications','AdminController@notifications')->name('notifications');
        Route::get('notification/all','AdminController@notificationReadAll')->name('notification.read.all');


        // Users Manager
        Route::get('users', 'ManageUsersController@allUsers')->name('users.all');
        Route::get('users_export', 'ManageUsersController@exportAllUsers')->name('users.export.all');
        Route::get('users/active', 'ManageUsersController@activeUsers')->name('users.active');
        Route::get('users/banned', 'ManageUsersController@bannedUsers')->name('users.banned');
        Route::get('users/data/reject', 'ManageUsersController@rejectDataUsers')->name('users.datareject');
        Route::get('users/data/verified', 'ManageUsersController@verifiedDataUsers')->name('users.dataverified');
        Route::get('users/data/waiting-for-verification', 'ManageUsersController@verificationDataUsers')->name('users.dataverification');
        Route::get('users/email-verified', 'ManageUsersController@emailVerifiedUsers')->name('users.emailVerified');
        Route::get('users/email-unverified', 'ManageUsersController@emailUnverifiedUsers')->name('users.emailUnverified');
        Route::get('users/sms-unverified', 'ManageUsersController@smsUnverifiedUsers')->name('users.smsUnverified');
        Route::get('users/sms-verified', 'ManageUsersController@smsVerifiedUsers')->name('users.smsVerified');
        Route::get('user/login/{id}', 'ManageUsersController@login')->name('users.login');
        Route::post('user/verify/{id}', 'ManageUsersController@verify')->name('users.verify');
        Route::post('user/reject/{id}', 'ManageUsersController@reject')->name('users.reject');

        Route::get('users/{scope}/search', 'ManageUsersController@search')->name('users.search');
        Route::get('user/detail/{id}', 'ManageUsersController@detail')->name('users.detail');
        Route::get('user/gold_invest', 'ManageUsersController@userGold')->name('invest.gdetail');
        Route::get('user/gold_invest_export', 'ManageUsersController@exportUserGold')->name('invest.gdetail.export');
        Route::get('user/gold_invest_detail/{id}', 'ManageUsersController@goldDetail')->name('users.invest.detail');
        Route::get('user/referral/{id}', 'ManageUsersController@userRef')->name('users.ref');
        Route::post('user/update/{id}', 'ManageUsersController@update')->name('users.update');
        Route::post('user/updateNik/{id}', 'ManageUsersController@updateNik')->name('users.updateNik');
        Route::post('user/rek/{id}', 'ManageUsersController@rek')->name('users.rek');
        Route::post('user/dinaran/{id}', 'ManageUsersController@dinaran')->name('users.dinaran');
        Route::post('user/add-sub-balance/{id}', 'ManageUsersController@addSubBalance')->name('users.addSubBalance');
        Route::post('user/set-user-placement/{id}', 'ManageUsersController@setUserPlacement')->name('users.setUserPlacement');
        Route::post('user/update_counting/{id}', 'ManageUsersController@updateCounting')->name('users.updateCounting');
        Route::post('user/add_left/{id}', 'ManageUsersController@snipperLeft')->name('users.addleft');
        Route::post('user/add_right/{id}', 'ManageUsersController@snipperRight')->name('users.addright');
        
        //akun dinaran
        Route::post('user/editDinaran', 'ManageUsersController@editDinaran')->name('users.edit.dinaran');

        Route::get('user/send-email/{id}', 'ManageUsersController@showEmailSingleForm')->name('users.email.single');
        Route::post('user/send-email/{id}', 'ManageUsersController@sendEmailSingle')->name('users.email.single');
        Route::get('user/transactions/{id}', 'ManageUsersController@transactions')->name('users.transactions');
        Route::get('user/deposits/{id}', 'ManageUsersController@deposits')->name('users.deposits');
        Route::get('user/deposits/via/{method}/{type?}/{userId}', 'ManageUsersController@depViaMethod')->name('users.deposits.method');
        Route::get('user/withdrawals/{id}', 'ManageUsersController@withdrawals')->name('users.withdrawals');
        Route::get('user/withdrawals/via/{method}/{type?}/{userId}', 'ManageUsersController@withdrawalsViaMethod')->name('users.withdrawals.method');
        // Login History
        Route::get('users/login/history/{id}', 'ManageUsersController@userLoginHistory')->name('users.login.history.single');
        Route::get('users/send-email', 'ManageUsersController@showEmailAllForm')->name('users.email.all');
        Route::post('users/send-email', 'ManageUsersController@sendEmailAll')->name('users.email.send');
        
        Route::get('user/completed/survey/{id}', 'ManageUsersController@survey')->name('users.survey');
        Route::post('user/add-bro-qty/{id}', 'ManageUsersController@addBROqty')->name('users.addbroqty');
        Route::post('user/add-frer-bro-qty/{id}', 'ManageUsersController@addFreeBROqty')->name('users.addfreebroqty');
        Route::get('user/matriks_potential', 'ManageUsersController@userMatriks')->name('user.matriks');
        
        //upgrade users
        Route::post('user/upgrade-plan/{id}', 'ManageUsersController@uplevelBRODP')->name('users.upgradelevelBRODP');


        Route::get('user/balance', 'ManageUsersController@userBalance')->name('users.balance');
        Route::get('users_exbalance', 'ManageUsersController@exbalance')->name('users.export.balance');

        
        Route::get('user/custom_order', 'CorderController@adminIndex')->name('custom.order');
        Route::get('user/custom_order/details/{id}', 'CorderController@detail')->name('corder.details');
        Route::post('user/custom_order/approve', 'CorderController@app')->name('corder.approve');
        Route::post('user/custom_order/reject', 'CorderController@rej')->name('corder.reject');
        Route::post('user/custom_order/update', 'CorderController@upd')->name('corder.update');
        
        Route::get('leaderboard', 'ManageUsersController@topLeader')->name('topleader');
        Route::get('user_reward/list', 'ManageUsersController@userRewardList')->name('user.reward');
        Route::get('user_reward/scan', 'ManageUsersController@userRewardScan')->name('user.reward.scan');

        // mlm plan
        Route::get('plans', 'MlmController@plan')->name('plan');
        Route::post('plan/store', 'MlmController@planStore')->name('plan.store');

        Route::post('plan/update', 'MlmController@planUpdate')->name('plan.update');
        Route::get('plan/delete/{id}', 'MlmController@planDelete')->name('plan.delete');
        Route::get('/plan/tesapi/{id}', 'MlmController@tesApi')->name('plan.tesapi');

        // mlm plan
        Route::get('product', 'ProductController@products')->name('product');
        Route::get('product/special', 'ProductController@productspecial')->name('productspecial');
        Route::post('products/store', 'ProductController@productsStore')->name('products.store');
        
        Route::post('products/update', 'ProductController@productsUpdate')->name('products.update');
        Route::post('products/update/stok', 'ProductController@upStok')->name('products.updatestok');
        
        //gacha
        Route::get('gacha', 'GachamasterController@index')->name('gacha');
        Route::post('gacha/update', 'GachamasterController@update')->name('gacha.update');
        Route::get('gacha/item/{id}', 'GachamasterController@detail')->name('gacha.detail');
        Route::post('gacha/detail/update', 'GachamasterdetailController@update')->name('gacha.update.detail');
        Route::post('gacha/detail/store', 'GachamasterdetailController@add')->name('gacha.add.detail');
        Route::post('gacha/status/{id}', 'GachamasterController@status')->name('gacha.status');
        
        Route::get('gacha/plot/detail/{id}', 'GachamasterController@plot')->name('gacha.user.plot');
        Route::post('gacha/plot/detail', 'GachamasterController@addplotuser')->name('gacha.user.plot.add');

        Route::get('redeem', 'RedeemmasterController@index')->name('redeem');
        Route::post('redeem/update', 'RedeemmasterController@update')->name('redeem.update');
        Route::get('redeem/item/{id}', 'RedeemmasterController@detail')->name('redeem.detail');
        Route::post('redeem/detail/update', 'RedeemmasterdetailController@update')->name('redeem.update.detail');
        Route::post('redeem/detail/store', 'RedeemmasterdetailController@add')->name('redeem.add.detail');
        Route::post('redeem/status/{id}', 'RedeemmasterController@status')->name('redeem.status');
    

        Route::get('category', 'CategoryController@index')->name('category');
        Route::post('category/store', 'CategoryController@store')->name('category.store');
        Route::post('category/edit', 'CategoryController@edit')->name('category.edit');
        Route::get('category/delete/{id}', 'CategoryController@delete')->name('category.delete');

        Route::resource('bonus-reward', 'BonusRewardController');
        Route::post('bonus-reward/update', 'BonusRewardController@upreward')->name('reward.update');
        
        
        //voucher
        Route::get('voucher', 'VoucherController@index')->name('voucher');
        Route::post('voucher/store', 'VoucherController@store')->name('voucher.store');
        Route::post('voucher/update', 'VoucherController@update')->name('voucher.update');


        // matching bonus
        Route::post('matching-bonus/update', 'MlmController@matchingUpdate')->name('matching-bonus.update');

        // tree
        Route::get('/tree/{id}', 'ManageUsersController@tree')->name('users.single.tree');
        Route::get('/user/tree/{user}', 'ManageUsersController@otherTree')->name('users.other.tree');
        Route::get('/user/tree/search', 'ManageUsersController@otherTree')->name('users.other.tree.search');

        Route::get('notice', 'GeneralSettingController@noticeIndex')->name('setting.notice');
        Route::post('notice/update', 'GeneralSettingController@noticeUpdate')->name('setting.notice.update');




        // Subscriber
        Route::get('subscriber', 'SubscriberController@index')->name('subscriber.index');
        Route::get('subscriber/send-email', 'SubscriberController@sendEmailForm')->name('subscriber.sendEmail');
        Route::post('subscriber/remove', 'SubscriberController@remove')->name('subscriber.remove');
        Route::post('subscriber/send-email', 'SubscriberController@sendEmail')->name('subscriber.sendEmail');


        // Deposit Gateway
        Route::name('gateway.')->prefix('gateway')->group(function(){
            // Automatic Gateway
            Route::get('automatic', 'GatewayController@index')->name('automatic.index');
            Route::get('automatic/edit/{alias}', 'GatewayController@edit')->name('automatic.edit');
            Route::post('automatic/update/{code}', 'GatewayController@update')->name('automatic.update');
            Route::post('automatic/remove/{code}', 'GatewayController@remove')->name('automatic.remove');
            Route::post('automatic/activate', 'GatewayController@activate')->name('automatic.activate');
            Route::post('automatic/deactivate', 'GatewayController@deactivate')->name('automatic.deactivate');



            // Manual Methods
            Route::get('manual', 'ManualGatewayController@index')->name('manual.index');
            Route::get('manual/new', 'ManualGatewayController@create')->name('manual.create');
            Route::post('manual/new', 'ManualGatewayController@store')->name('manual.store');
            Route::get('manual/edit/{alias}', 'ManualGatewayController@edit')->name('manual.edit');
            Route::post('manual/update/{id}', 'ManualGatewayController@update')->name('manual.update');
            Route::post('manual/activate', 'ManualGatewayController@activate')->name('manual.activate');
            Route::post('manual/deactivate', 'ManualGatewayController@deactivate')->name('manual.deactivate');
        });


        // DEPOSIT SYSTEM
        Route::name('deposit.')->prefix('deposit')->group(function(){
            Route::get('/', 'DepositController@deposit')->name('list');
            Route::get('pending', 'DepositController@pending')->name('pending');
            Route::get('rejected', 'DepositController@rejected')->name('rejected');
            Route::get('approved', 'DepositController@approved')->name('approved');
            Route::get('successful', 'DepositController@successful')->name('successful');
            Route::get('details/{id}', 'DepositController@details')->name('details');

            Route::post('reject', 'DepositController@reject')->name('reject');
            Route::post('approve', 'DepositController@approve')->name('approve');
            Route::get('via/{method}/{type?}', 'DepositController@depViaMethod')->name('method');
            Route::get('/{scope}/search', 'DepositController@search')->name('search');
            Route::get('/{scope}/searchStatus', 'DepositController@searchStatus')->name('searchStatus');
            Route::get('date-search/{scope}', 'DepositController@dateSearch')->name('dateSearch');
            
            Route::get('export', 'DepositController@export')->name('export');

        });


        // WITHDRAW SYSTEM
        Route::name('withdraw.')->prefix('withdraw')->group(function(){
            Route::get('pending', 'WithdrawalController@pending')->name('pending');
            Route::get('approved', 'WithdrawalController@approved')->name('approved');
            Route::get('rejected', 'WithdrawalController@rejected')->name('rejected');
            Route::get('log', 'WithdrawalController@log')->name('log');
            Route::get('via/{method_id}/{type?}', 'WithdrawalController@logViaMethod')->name('method');
            Route::get('{scope}/search', 'WithdrawalController@search')->name('search');
            Route::get('date-search/{scope}', 'WithdrawalController@dateSearch')->name('dateSearch');
            Route::get('details/{id}', 'WithdrawalController@details')->name('details');
            Route::post('approve', 'WithdrawalController@approve')->name('approve');
            Route::post('reject', 'WithdrawalController@reject')->name('reject');


            // Withdraw Method
            Route::get('method/', 'WithdrawMethodController@methods')->name('method.index');
            Route::get('method/create', 'WithdrawMethodController@create')->name('method.create');
            Route::post('method/create', 'WithdrawMethodController@store')->name('method.store');
            Route::get('method/edit/{id}', 'WithdrawMethodController@edit')->name('method.edit');
            Route::post('method/edit/{id}', 'WithdrawMethodController@update')->name('method.update');
            Route::post('method/activate', 'WithdrawMethodController@activate')->name('method.activate');
            Route::post('method/deactivate', 'WithdrawMethodController@deactivate')->name('method.deactivate');
            Route::get('wd/export', 'WithdrawalController@export')->name('wdexport');
        });

        // Report
        Route::get('report/referral-commission', 'ReportController@refCom')->name('report.refCom');
        Route::get('report/binary-commission', 'ReportController@binary')->name('report.binaryCom');
        Route::get('report/repeat-commission', 'ReportController@repeat')->name('report.repeatCom');
        Route::get('report/invest', 'ReportController@invest')->name('report.invest');
        Route::get('report/redeem/fili', 'ReportController@redeem')->name('report.redeem');
        Route::get('report/redeem/saka', 'ReportController@redeem2')->name('report.redeem2');

        Route::get('report/bv-log', 'ReportController@bvLog')->name('report.bvLog');
        Route::get('report/bv-log/{id}', 'ReportController@singleBvLog')->name('report.single.bvLog');

        Route::get('report/transaction', 'ReportController@transaction')->name('report.transaction');
        Route::get('report/transaction/search', 'ReportController@transactionSearch')->name('report.transaction.search');


        Route::get('report/login/history', 'ReportController@loginHistory')->name('report.login.history');
        Route::get('report/login/ipHistory/{ip}', 'ReportController@loginIpHistory')->name('report.login.ipHistory');
        
        Route::get('report/export', 'ReportController@export')->name('report.export');


        // Admin Support
        Route::get('tickets', 'SupportTicketController@tickets')->name('ticket');
        Route::get('tickets/pending', 'SupportTicketController@pendingTicket')->name('ticket.pending');
        Route::get('tickets/closed', 'SupportTicketController@closedTicket')->name('ticket.closed');
        Route::get('tickets/answered', 'SupportTicketController@answeredTicket')->name('ticket.answered');
        Route::get('tickets/view/{id}', 'SupportTicketController@ticketReply')->name('ticket.view');
        Route::post('ticket/reply/{id}', 'SupportTicketController@ticketReplySend')->name('ticket.reply');
        Route::get('ticket/download/{ticket}', 'SupportTicketController@ticketDownload')->name('ticket.download');
        Route::post('ticket/delete', 'SupportTicketController@ticketDelete')->name('ticket.delete');


        // Language Manager
        Route::get('/language', 'LanguageController@langManage')->name('language.manage');
        Route::post('/language', 'LanguageController@langStore')->name('language.manage.store');
        Route::post('/language/delete/{id}', 'LanguageController@langDel')->name('language.manage.del');
        Route::post('/language/update/{id}', 'LanguageController@langUpdatepp')->name('language.manage.update');
        Route::get('/language/edit/{id}', 'LanguageController@langEdit')->name('language.key');
        Route::post('/language/import', 'LanguageController@langImport')->name('language.import_lang');



        Route::post('language/store/key/{id}', 'LanguageController@storeLanguageJson')->name('language.store.key');
        Route::post('language/delete/key/{id}', 'LanguageController@deleteLanguageJson')->name('language.delete.key');
        Route::post('language/update/key/{id}', 'LanguageController@updateLanguageJson')->name('language.update.key');



        // General Setting
        Route::get('general-setting', 'GeneralSettingController@index')->name('setting.index');
        Route::post('general-setting', 'GeneralSettingController@update')->name('setting.update');

        // Logo-Icon
        Route::get('setting/logo-icon', 'GeneralSettingController@logoIcon')->name('setting.logo_icon');
        Route::post('setting/logo-icon', 'GeneralSettingController@logoIconUpdate')->name('setting.logo_icon');

        // Plugin
        Route::get('extensions', 'ExtensionController@index')->name('extensions.index');
        Route::post('extensions/update/{id}', 'ExtensionController@update')->name('extensions.update');
        Route::post('extensions/activate', 'ExtensionController@activate')->name('extensions.activate');
        Route::post('extensions/deactivate', 'ExtensionController@deactivate')->name('extensions.deactivate');


        // Email Setting
        Route::get('email-template/global', 'EmailTemplateController@emailTemplate')->name('email.template.global');
        Route::post('email-template/global', 'EmailTemplateController@emailTemplateUpdate')->name('email.template.global');
        Route::get('email-template/setting', 'EmailTemplateController@emailSetting')->name('email.template.setting');
        Route::post('email-template/setting', 'EmailTemplateController@emailSettingUpdate')->name('email.template.setting');
        Route::get('email-template/index', 'EmailTemplateController@index')->name('email.template.index');
        Route::get('email-template/{id}/edit', 'EmailTemplateController@edit')->name('email.template.edit');
        Route::post('email-template/{id}/update', 'EmailTemplateController@update')->name('email.template.update');
        Route::post('email-template/send-test-mail', 'EmailTemplateController@sendTestMail')->name('email.template.sendTestMail');


        // SMS Setting
        Route::get('sms-template/global', 'SmsTemplateController@smsSetting')->name('sms.template.global');
        Route::post('sms-template/global', 'SmsTemplateController@smsSettingUpdate')->name('sms.template.global');
        Route::get('sms-template/index', 'SmsTemplateController@index')->name('sms.template.index');
        Route::get('sms-template/edit/{id}', 'SmsTemplateController@edit')->name('sms.template.edit');
        Route::post('sms-template/update/{id}', 'SmsTemplateController@update')->name('sms.template.update');
        Route::post('email-template/send-test-sms', 'SmsTemplateController@sendTestSMS')->name('sms.template.sendTestSMS');

        // Exchange
        Route::get('exchange', 'ExchangeController@index')->name('exchange');
        Route::post('exchange/reject/{id}', 'ExchangeController@reject')->name('exchange.reject');
        Route::post('exchange/accept/{id}', 'ExchangeController@verify')->name('exchange.accept');

        Route::get('delivery', 'DeliveryController@index')->name('delivery');
        Route::get('export_delivery', 'DeliveryController@export')->name('export.delivery');
        
        Route::post('delivery/deliver', 'DeliveryController@delivery')->name('deliver.deliver');
        Route::get('bro_delivery', 'BrodevController@index')->name('BroDelivery');
        Route::get('bro_delivery/pickup', 'BrodevController@index2')->name('BroDelivery.pickup');
        Route::get('bro_delivery/konversi', 'BrodevController@index3')->name('BroDelivery.dinaran');
        Route::post('bro_delivery/deliver', 'BrodevController@delivery')->name('BroDeliver.deliver');
        Route::get('brodev/search_delivery', 'BrodevController@search')->name('BroDelivery.search');
        Route::get('brodev/search_pickup', 'BrodevController@search2')->name('BroDelivery.search2');
        Route::get('export_bro_delivery', 'BrodevController@export')->name('export.BROdelivery');
        Route::get('export_bro_pickup', 'BrodevController@export2')->name('export.BROpickup');
        
        Route::post('resend/email/{user_id}/{brodevtrx}', 'BrodevController@resendEmail')->name('resend.email');
        Route::post('accept/email/{user_id}/{brodevtrx}', 'BrodevController@acceptDinaran')->name('accept.dinaran');
        Route::post('reject/email/{user_id}/{brodevtrx}', 'BrodevController@rejectDinaran')->name('reject.dinaran');

        // SEO
        Route::get('seo', 'FrontendController@seoEdit')->name('seo');


        // Frontend
        Route::name('frontend.')->prefix('frontend')->group(function () {

            Route::get('templates', 'FrontendController@templates')->name('templates');
            Route::post('templates', 'FrontendController@templatesActive')->name('templates.active');

            Route::get('frontend-sections/{key}', 'FrontendController@frontendSections')->name('sections');
            Route::post('frontend-content/{key}', 'FrontendController@frontendContent')->name('sections.content');
            Route::get('frontend-element/{key}/{id?}', 'FrontendController@frontendElement')->name('sections.element');
            Route::post('remove', 'FrontendController@remove')->name('remove');

            // Page Builder
            Route::get('manage-pages', 'PageBuilderController@managePages')->name('manage.pages');
            Route::post('manage-pages', 'PageBuilderController@managePagesSave')->name('manage.pages.save');
            Route::post('manage-pages/update', 'PageBuilderController@managePagesUpdate')->name('manage.pages.update');
            Route::post('manage-pages/delete', 'PageBuilderController@managePagesDelete')->name('manage.pages.delete');
            Route::get('manage-section/{id}', 'PageBuilderController@manageSection')->name('manage.section');
            Route::post('manage-section/{id}', 'PageBuilderController@manageSectionUpdate')->name('manage.section.update');
        });
    });
});




/*
|--------------------------------------------------------------------------
| Start User Area
|--------------------------------------------------------------------------
*/


Route::name('user.')->group(function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->middleware('regStatus');


    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/code-verify', 'Auth\ForgotPasswordController@codeVerify')->name('password.code_verify');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/verify-code', 'Auth\ForgotPasswordController@verifyCode')->name('password.verify-code')->middleware('throttle:5,120');;
});


Route::name('user.')->prefix('user')->group(function () {

    Route::get('/Product', 'ProductController@productIndex')->name('product.index');
    Route::get('/Product/detail/{id}', 'ProductController@productDetail')->name('product.detail');

    Route::middleware('auth')->group(function () {
        Route::get('authorization', 'AuthorizationController@authorizeForm')->name('authorization');
        Route::get('resend-verify', 'AuthorizationController@sendVerifyCode')->name('send_verify_code');
        Route::post('verify-email', 'AuthorizationController@emailVerification')->name('verify_email');
        Route::post('verify-sms', 'AuthorizationController@smsVerification')->name('verify_sms');
        Route::post('verify-g2fa', 'AuthorizationController@g2faVerification')->name('go2fa.verify');

        Route::middleware(['checkStatus'])->group(function () {
            Route::get('dashboard', 'UserController@home')->name('home');
            Route::get('referals', 'UserController@ref')->name('ref.tree');

            Route::get('profile-setting', 'UserController@profile')->name('profile-setting');
            Route::post('profile-setting', 'UserController@submitProfile');
            Route::post('edit_rekening', 'UserController@edit_rekening')->name('edit_rekening');
            Route::post('add_rekening', 'UserController@add_rekening')->name('add_rekening');
            Route::get('change-password', 'UserController@changePassword')->name('change-password');
            Route::post('change-password', 'UserController@submitPassword');

            Route::post('add_address', 'AlamatController@add_address')->name('add_address');
            Route::post('edit_address', 'AlamatController@edit_address')->name('edit_address');

            //Claim Free Bro
            Route::post('claimbro', 'UserController@claimBro')->name('claim.bro1');
            Route::post('claimbro2', 'UserController@claimBro2')->name('claim.bro2');

            Route::get('reward', 'UrewardController@userReward')->name('reward');
            Route::get('reward/print/{trx}', 'UrewardController@printTicket')->name('ticket.print');
            //Claimreward
            Route::post('claim_reward/{id}', 'UrewardController@claimReward')->name('claim.reward');
            Route::post('claim_rewardup/{id}', 'UrewardController@claimRewardUp')->name('claim.rewardup');

            //2FA
            Route::get('twofactor', 'UserController@show2faForm')->name('twofactor');
            Route::post('twofactor/enable', 'UserController@create2fa')->name('twofactor.enable');
            Route::post('twofactor/disable', 'UserController@disable2fa')->name('twofactor.disable');
            Route::get('login/history', 'UserController@userLoginHistory')->name('login.history');

            //plan
            Route::get('/plan', 'PlanController@planIndex')->name('plan.index');
            Route::get('/plan/bro-pack-select/bro-{user}-{trx}', 'PlanController@selectType')->name('plan.select.type');
            Route::post('/plan/bro-pack-select/sumbit', 'PlanController@selectTypeSubmit')->name('plan.select.submit');

            // Route::get('/Product', 'ProductController@productIndex')->name('product.index');
            Route::get('/Product-special/{bro}', 'ProductController@productSpecial')->name('product.product_special');
            
            // Route::post('/plan', 'PlanController@planStore')->name('plan.purchase');
            Route::get('/referral-log', 'UserController@referralCom')->name('referral.log');

            // Route::get('/binary-log', 'PlanController@binaryCom')->name('binary.log');
            Route::get('/binary-summery', 'PlanController@binarySummery')->name('binary.summery');
            Route::get('/bv-log', 'PlanController@bvlog')->name('bv.log');
            // Route::get('/referrals', 'PlanController@myRefLog')->name('my.ref');
            Route::get('/tree', 'PlanController@myTree')->name('my.tree');
            Route::get('/tree/{user}', 'PlanController@otherTree')->name('other.tree');
            Route::get('/tree/search', 'PlanController@otherTree')->name('other.tree.search');

            //balance transfer
            Route::get('/transfer', 'UserController@indexTransfer')->name('balance.transfer');
            Route::post('/transfer', 'UserController@balanceTransfer')->name('balance.transfer.post');
            Route::post('/search-user', 'UserController@searchUser')->name('search.user');

            //Report
            // Route::get('report/deposit/log', 'UserReportController@depositHistory')->name('report.deposit');
            // Route::get('report/invest/log', 'UserReportController@investLog')->name('report.invest');
            // Route::get('report/transactions/log', 'UserReportController@transactions')->name('report.transactions');
            // Route::get('report/withdraw/log', 'UserReportController@withdrawLog')->name('report.withdraw');
            // Route::get('report/referral/commission', 'UserReportController@refCom')->name('report.refCom');
            // Route::get('report/binary/commission', 'UserReportController@binaryCom')->name('report.binaryCom');

            // Deposit
            // Route::any('deposit', 'Gateway\PaymentController@deposit')->name('deposit');
            Route::post('deposit/insert', 'Gateway\PaymentController@depositInsert')->name('deposit.insert');
            Route::get('deposit/preview', 'Gateway\PaymentController@depositPreview')->name('deposit.preview');
            Route::get('deposit/payment/{trx}', 'Gateway\PaymentController@payment')->name('deposit.payment');
            Route::get('deposit/confirm', 'Gateway\PaymentController@depositConfirm')->name('deposit.confirm');
            Route::post('deposit/manual', 'Gateway\PaymentController@manualDepositConfirm')->name('deposit.manual.confirm');
            // Route::post('deposit/manual', 'Gateway\PaymentController@manualDepositUpdate')->name('deposit.manual.update');
            Route::get('deposit/history', 'UserController@depositHistory')->name('deposit.history');
            // Route::get('thank-you', 'Gateway\PaymentController@thankyou')->name('deposit.manual.thankyou');
            // Route::get('cancel-payment', 'Gateway\PaymentController@cancelpayment')->name('deposit.manual.cancel');
            // Route::get('callback-url', 'Gateway\PaymentController@callback')->name('deposit.manual.callback');
            
            // Withdraw
            // Route::get('withdraw', 'UserController@withdrawMoney')->name('withdraw');
            Route::post('withdraw', 'UserController@withdrawStore')->name('withdraw.money');
            Route::get('withdraw/preview', 'UserController@withdrawPreview')->name('withdraw.preview');
            Route::post('withdraw/preview', 'UserController@withdrawSubmit')->name('withdraw.submit');
            Route::get('withdraw/history', 'UserController@withdrawLog')->name('withdraw.history');
            Route::get('gacha/history', 'UserController@redeemLog')->name('report.redeem.history');
            Route::get('redeem/history', 'UserController@redeem2Log')->name('report.redeem.history.saka');

            //Survey
            Route::get('/survey', 'UserController@surveyAvailable')->name('survey');
            Route::get('/survey/{id}/questions', 'UserController@surveyQuestions')->name('survey.questions');
            Route::post('/survey/answers', 'UserController@surveyQuestionsAnswers')->name('survey.questions.answers');

            
                Route::get('gold_invest', 'UserController@goldInvest')->name('gold.invest');
                Route::post('/Product-purchase', 'ProductController@productPurchase')->name('product.purchase');
                Route::post('/Product-custom', 'CorderController@productCustom')->name('product.custom');
                Route::post('/Product-rcustom', 'CorderController@rproductCustom')->name('product.rcustom');
                Route::post('/otp/send', 'OTPVerificationController@sendOTP')->name('otp.product');
                // Route::post('/otp/verify', [OTPVerificationController::class, 'verifyOTP']);

                // Route::get('withdraw', 'UserController@withdrawMoney')->name('withdraw');
                Route::any('deposit', 'Gateway\PaymentController@deposit')->name('deposit');
                Route::get('report/deposit/log', 'UserReportController@depositHistory')->name('report.deposit');
                Route::get('report/invest/log', 'UserReportController@investLog')->name('report.invest');
                Route::get('report/transactions/log', 'UserReportController@transactions')->name('report.transactions');
                Route::get('report/withdraw/log', 'UserReportController@withdrawLog')->name('report.withdraw');
                Route::get('report/referral/commission', 'UserReportController@refCom')->name('report.refCom');
                Route::get('report/binary/commission', 'UserReportController@binaryCom')->name('report.binaryCom');
                Route::get('report/repeat/commission', 'UserReportController@repeatCom')->name('report.repeatCom');
                Route::get('report/exchange/log', 'UserReportController@exchangeLog')->name('report.exchangeLog');
                Route::get('report/delivery/log', 'UserReportController@deliveryLog')->name('report.deliveryLog');
                Route::get('report/Brodelivery/log', 'UserReportController@BroDeliveryLog')->name('report.BroDeliveryLog');

                Route::post('/plan', 'PlanController@planStore')->name('plan.purchase');
                Route::post('/plan/cancel/{id}', 'PlanController@cancelPlan')->name('plan.cancel');
                Route::post('/buyBRO', 'PlanController@buyBROStore')->name('plan.bropurchase');
                Route::post('/repeat_order', 'PlanController@repeatOrder')->name('plan.repeat.order');

                Route::middleware(['checkPaid'])->group(function () {
                    Route::get('/referrals', 'PlanController@myRefLog')->name('my.ref');
                    Route::get('/manage_user', 'UserController@user_boom')->name('user_boom');
                    Route::get('/tree', 'PlanController@myTree')->name('my.tree');
                    Route::get('/request/special-product/{id}','UserController@reqSpecialProd')->name('request.sprod');
                });
                
            Route::middleware(['checkKyc'])->group(function () {
                Route::get('withdraw', 'UserController@withdrawMoney')->name('withdraw');
            });

            Route::post('dinaran/store', 'DinaranAccController@store')->name('dinaran.store');
            Route::post('dinaran/withdraw', 'DinaranAccController@withdraw')->name('dinaran.withdraw');

            Route::post('gold_exchange', 'UserController@goldExchange')->name('gold.exchange');
            Route::post('gold_delivery', 'SendgoldController@goldDelivery')->name('gold.delivery');
            Route::post('gold_delivery_cart', 'DevcartController@addToCart')->name('gold.delcart');
            Route::get('delete_cart/{id}', 'DevcartController@delCart')->name('gold.deletecart');
            Route::get('delivery_cart', 'DevcartController@ind')->name('delivery.cart');
            Route::post('update_cart', 'DevcartController@updCart')->name('gold.updatecart');
            Route::post('checkout_cart', 'DevcartController@checkOut')->name('gold.checkoutcart');
            Route::get('checkout_preview', 'DevcartController@checkOutPreview')->name('sg.preview');
            Route::post('checkout', 'DevcartController@checkOutConfirm')->name('gold.checkoutconfirm');

            Route::get('/cek_pos/{id}', 'UserController@cek_pos')->name('cek_pos');
            Route::get('/cek_tree/{id}', 'UserController@cek_tree')->name('cek_tree');
            Route::post('/user', 'UserController@user')->name('user');

            Route::get('/verification','UserController@verification')->name('verification');
            Route::post('/submitVerification','UserController@submitVerification')->name('submitVerification');
            
            
            
            Route::post('/redeem/store','UsergachaController@store')->name('gacha.store');
            Route::post('/redeem/storev2','UsergachaController@storev2')->name('gacha.storev2');
            Route::post('/redeem/claimv2','UsergachaController@claimv2')->name('gacha.claimv2');
            Route::get('/fili_got_it','UsergachaController@filiGotIt')->name('gacha.fili.got.it');
            Route::get('/fili_got_it_v2','UsergachaController@filiGotItv2')->name('gacha.fili.got.itv2');
            
            // Route::get('/saka_vaganza','UserredeemController@saka_vaganza')->name('saka.vaganza');
            Route::post('/claim','UserredeemController@claim')->name('saka.vaganza.claim');

        });
    });
});

Route::post('/check/referral', 'SiteController@CheckUsername')->name('check.referral');
Route::post('/check/referralbro', 'SiteController@CheckBro')->name('check.referralbro');
Route::post('/check/brodev', 'SiteController@CheckBrodev')->name('check.brodev');
Route::post('/get/user/position', 'SiteController@userPosition')->name('get.user.position');

Route::get('thank-you', 'Gateway\PaymentController@thankyou')->name('deposit.manual.thankyou');
Route::get('cancel-payment', 'Gateway\PaymentController@cancelpayment')->name('deposit.manual.cancel');
Route::post('callback-url', 'Gateway\PaymentController@callback')->name('deposit.manual.callback');
Route::post('invoice_callback_url', 'Gateway\PaymentController@callback2')->name('deposit.manual.callback2');
Route::get('success/{id}', 'Gateway\PaymentController@successPage')->name('deposit.success');
Route::get('failed/{id}', 'Gateway\PaymentController@failedPage')->name('deposit.success');
Route::get('direct/{id}', 'Gateway\PaymentController@planSuccess')->name('tes');
Route::get('repeatorder/{id}', 'Gateway\PaymentController@roSuccess')->name('tesro');

Route::post('disbursement_callback_url', 'Admin\WithdrawalController@wdcallback')->name('withdraw.callback');

Route::post('subscriber', 'SiteController@subscriberStore')->name('subscriber.store');
// Policy Details
Route::get('policy/{slug}/{id}', 'SiteController@policyDetails')->name('policy.details');

Route::get('/contact', 'SiteController@contact')->name('contact');
Route::post('/contact', 'SiteController@contactSubmit')->name('contact.send');
Route::get('/change/{lang?}', 'SiteController@changeLanguage')->name('lang');

Route::get('/blog', 'SiteController@blog')->name('blog');
Route::get('/blog/details/{slug}/{id}', 'SiteController@singleBlog')->name('singleBlog');

// Route::get('/home2', 'SiteController@index2')->name('home2');
// Route::get('/', 'SiteController@index')->name('home');
// Route::get('/{slug}', 'SiteController@pages')->name('pages');
Route::get('/', function () {
    // return redirect('/login');
    return redirect()->route('user.login');
});

Route::get('placeholder-image/{size}', 'SiteController@placeholderImage')->name('placeholderImage');
Route::get('links/{slug}', 'SiteController@links')->name('links');

Route::get('/get/city', 'UserController@getCity')->name('city');
Route::get('/get/province', 'UserController@getProv')->name('province');

Route::get('/Product/detail/{id}', 'ProductController@productDetailGuest')->name('product.detail.guest');
Route::get('/Product', 'ProductController@productSpGuest')->name('product.product_special');

Route::get('/join', 'PlanController@refJoin')->name('plan.join');

