<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.token')->group(function () {
//     return $request->user();
Route::post('/upload-avatar', 'UserController@apiavatarprofile');
Route::post('/upload-kyc', 'UserController@apiimagekyc');
Route::post('/mail/purchase_product', 'ProductController@mailpp');
Route::post('/mail/purchase_plan', 'PlanController@mailpp');
});

Route::post('/repeat_order/com', 'ProductController@repeatordercomm');
Route::post('/data/level_referall', 'ProductController@datarefer');

Route::post('/migrate/gv','UserController@gvRegister');