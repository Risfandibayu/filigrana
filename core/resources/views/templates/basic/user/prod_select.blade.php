@extends($activeTemplate . 'user.layouts.app')

@section('panel')
<div class="row mb-none-30">

    <div class="col-lg-12 col-md-12 mb-30">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-50 border-bottom pb-2">@lang('BRO Delivery - Select Product')</h5>
                <form action="{{route('user.plan.select.submit')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">@lang('Delivery ID')</label>
                        <div class="col-lg-9">
                            <input type="text" name="trx" class="referral ref_name form-control form--control-2"
                                value="{{$fren->trx}}" id="trx" placeholder="@lang('Delivery ID')*"
                                required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">@lang('Time')</label>
                        <div class="col-lg-9">
                            <input type="text" name="time" class="referral ref_name form-control form--control-2"
                                value="{{showDateTime($fren->created_at)}}" id="trx" placeholder="@lang('Delivery ID')*"
                                required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">@lang('Delivery')</label>
                        <div class="col-lg-9">
                            @if (isset($fren->ship_method))
                            
                            <input type="text" name="date" class="referral ref_name form-control form--control-2"
                                value="@if ($fren->ship_method == 1)Pick Up Offline at {{ showDateTime($fren->pickupdate) }}
                                    @elseif($fren->ship_method == 3)Konversi Gramasi
                                    @else{{ Str::limit($fren->alamat,40) }}
                                    @endif" id="trx" placeholder="@lang('Delivery ID')*"
                                required readonly>
                            @else
                            <div class="col-12">
                                <label for="" class="col-form-label form-control-label">Select Shipping Method</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="shipmethod" value="1"
                                        id="flexRadioDefault1" required>
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Pick up at the office
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="shipmethod" value="2"
                                        id="flexRadioDefault2">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Delivery
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="shipmethod" value="3"
                                        id="flexRadioDefault3" @if ($dinaran == 0)
                                            disabled
                                        @endif>
                                    <label class="form-check-label" for="flexRadioDefault3" >
                                        Konversi Gramasi
                                    </label>
                                    @if ($dinaran == 0 )
                                        <span class="text-danger" style="font-size: 10px"> 
                                            @lang('(Akun Dinaran Belum di Masukan, Silahkan Masukan Akun Dinaran di Halaman Profile)')
                                        </span>
                                    @endif
                                </div>
                                <hr>
                            </div>
                            <div class="col-12 shipping" id="shipping" style="display: none">
                                <label for="" class="col-form-label form-control-label">Select BRO Pack Shipping Address <small>(If empty
                                        add shipping address first in profile setting)</small></label>
                                <select name="alamat" id="alamat" class="form-control form--control-2 alamat">
                                    <option value="" hidden selected>-- Select Address --</option>
                                    @foreach ($alamat as $item)
                                    <option value="{{$item->id}}">{{$item->nama_penerima .' | '.
                                        Str::limit($item->alamat,20)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 pickupdate" id="pickupdate" style="display: none">
                                <label for="" class="col-form-label form-control-label">Select BRO Pack Pick Up Date</label>
                                {{-- <input type="datetime-local" name="pickdate" class="form-control form--control-2"
                                    min="{{now()->format('Y-m-d H:i')}}"> --}}

                                <div class="row col-10">
                                    <div class="col-6">
                                        <input type="date" name="pickdate" id="pickdate"
                                            class="form-control form--control-2 pickdate"
                                            min="{{now()->format('Y-m-d')}}">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" name="picktime" id="picktime" autocomplete="off"
                                            class="form-control form--control-2 picktime timepicker" min="11:00"
                                            max="16:00" step="900" placeholder="11:00">
                                    </div>
                                    <span id="pick-error" class="pick-error" style="display: none">
                                        <span class="text-danger">
                                            @lang('This day not allowed, please select only on Tuesday, Wednesday and
                                            Thursday')
                                        </span>
                                    </span>
                                    <span id="pick-error2" class="pick-error2" style="display: none">
                                        <span class="text-danger">
                                            @lang('This date and time has been booked by another user, please choose another
                                            date and time')
                                        </span>
                                    </span>
                                </div>

                            </div>
                            @endif
                        </div>
                    </div>
                    @for ($i = 1; $i < $fren->bro_qty+1; $i++)
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">@lang('Select Product ') {{$i}}</label>
                        <div class="col-lg-9">
                            <select name="product[]" class="form-control form--control-2" id="" required>
                                <option value="" selected hidden>-- Select Product --</option>
                                <option value="1" >Microgold</option>
                                <option value="2" >Filigrana</option>
                                {{-- <option value="3" >Svassa</option>
                                <option value="4" >Vortiz</option>
                                <option value="5" >Alhambra</option> --}}
                                <option value="7">Product Promo</option>
                                {{-- <option value="6" @if ($dinaran == 0)
                                disabled
                            @endif>Konversi Gramasi
                                    @if ($dinaran == 0 )
                                    <span style="font-size: 10px;color:red !important"> 
                                        @lang('(Akun Dinaran Belum di Masukan, Silahkan Masukan Akun Dinaran di Halaman Profile)')
                                    </span>
                                @endif
                                    
                                </option> --}}
                            </select> 
                        </div>
                    </div>
                    @endfor

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label"></label>
                        <div class="col-lg-9">
                            <button type="submit" class="btn btn--primary btn-block btn-lg" onclick="this.form.submit(); this.disabled=true; this.value='Sending…';">@lang('Submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    (function($) {
        "use strict";
        // $('.position-test').text('');
        var oldPosition = '{{ old("position") }}';

        window.onload = function() {
            // let's go!
            $('.position-test').text('');
        }

        if(oldPosition){
            $('select[name=position]').removeAttr('disabled');
            $('.position').val(oldPosition);
        }

        var not_select_msg = $('.position-test').html();

        $(document).on('blur', '.ref_name', function() {
            var ref_id = this.form.elements['ref_name'].value;
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('check.referralbro')}}",
                data: {
                    'ref_id': ref_id,
                    '_token': token
                },
                success: function(data) {
                    // console.log();
                    if (data.success) {
                        $('.position').removeAttr('disabled');
                        $('.position-test').text('');
                        // console.log(this.form.elements['ref_name'].value);
                    } else {
                        // console.log('ss');
                        $('.position').attr('disabled', true);
                        $('.position-test').html(not_select_msg);
                    }
                    $("#ref").html(data.msg);
                }
            });
        });

       

        $(document).on('change', '.position', function() {
            updateHand();
        });

        function updateHand() {
            var pos = $('.position').val();
            var referrer_id = $('.upline').val();
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('get.user.position')}}",
                data: {
                    'referrer': referrer_id,
                    'position': pos,
                    '_token': token
                },
                error: function(data) {
                    $(".position-test").html(data.msg);
                }
            });
        }

        @if(@$country_code)
        $(`option[data-code={{ $country_code }}]`).attr('selected', '');
        @endif
        $('select[name=country_code]').change(function() {
            $('input[name=country]').val($('select[name=country_code] :selected').data('country'));
        }).change();

        function submitUserForm() {
            var response = grecaptcha.getResponse();
            if (response.length == 0) {
                document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">@lang("Captcha field is required.")</span>';
                return false;
            }
            return true;
        }

        function verifyCaptcha() {
            document.getElementById('g-recaptcha-error').innerHTML = '';
        }

        @if($general -> secure_password)
        $('input[name=password]').on('input', function() {
            var password = $(this).val();
            var capital = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
            var capital = capital.test(password);
            if (!capital) {
                $('.capital').removeClass('text--success');
            } else {
                $('.capital').addClass('text--success');
            }
            var number = /[123456790]/;
            var number = number.test(password);
            if (!number) {
                $('.number').removeClass('text--success');
            } else {
                $('.number').addClass('text--success');
            }
            var special = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
            var special = special.test(password);
            if (!special) {
                $('.special').removeClass('text--success');
            } else {
                $('.special').addClass('text--success');
            }

        });
        @endif

        $('.qty').on('keyup change',function(e) { 
        // alert(this);
        // console.log(this.form.elements['total'].value);
        // this.form.find('.total').val()
        // $('.total').val($('.qty').val() * $('.prices').val());
        this.form.elements['total'].value = this.form.elements['qty'].value * this.form.elements['prices'].value;
        });

        $('input[name=qtyy]').on('keyup change',function() {
            // alert('okl');
            $('input[name=totall]').val($('input[name=qtyy]').val() * $('input[name=pricess]').val());
        });


        $('.plan').on('change', function() {
            // alert( this.value );
            var id = this.value;
            if (id == 1) {
                    document.getElementById('shipping_method').style.display = 'flex';
                    $('input[name=shipmethod]').attr('required', true);
                    $('input[name=shipmethod]').prop('checked', false);
            }else{
                    document.getElementById('shipping_method').style.display = 'none';
                    $('input[name=shipmethod]').attr('required', false);
                    document.getElementById('shipping').style.display = 'none';
                    document.getElementById('pickupdate').style.display = 'none';

            }
        });


        // $(document).on('click', '.sbt', function() {
        //     doSomething(this.form.id);
        //     // console.log(this.form.id);
        // });

        // function doSomething(id) {
        //     $("button[type=submit]").attr('disabled', 'disabled');
        // // do your heavy stuff here
        //     $("#"+id).submit();
        //     // console.log(this.form);
        // }

        $(document).on('click', '[name=shipmethod]', function() {
            ship(this.value);
            // console.log(this.value);
            $('select[name=alamat]').val("");
            $('input[name=pickdate]').val("");
            $('input[name=picktime]').val("");
        });


        function ship(id) {
                if (id == 2) {
                    document.getElementById('shipping').style.display = 'flex';
                    document.getElementById('pickupdate').style.display = 'none';
                    $('select[name=alamat]').attr('required', true);
                    $('input[name=pickdate]').attr('required', false);
                    $('input[name=picktime]').attr('required', false);
                }
                else if (id == 1) {
                    document.getElementById('shipping').style.display = 'none';
                    document.getElementById('pickupdate').style.display = 'flex';
                    $('select[name=alamat]').attr('required', false);
                    $('input[name=pickdate]').attr('required', true);
                    $('input[name=picktime]').attr('required', true);
                }else{
                    document.getElementById('shipping').style.display = 'none';
                    document.getElementById('pickupdate').style.display = 'none';
                    $('select[name=alamat]').attr('required', false);
                    $('input[name=pickdate]').attr('required', false);
                    $('input[name=picktime]').attr('required', false);
                }
    
        };
        const picker = document.getElementById('pickdate');
        var pick = $('.pick-error').html();
        picker.addEventListener('input', function(e){
            var day = new Date(this.value).getUTCDay();
            if([5,0,6,1].includes(day)){
                e.preventDefault();
                this.value = '';
                // alert('This day not allowed');
                document.getElementById('pick-error').style.display = 'flex';
            }else{
                document.getElementById('pick-error').style.display = 'none';

            }
        });
        
        let myTimepicker = document.getElementById("picktime");
        // myTimepicker.addEventListener("change", function() {
        
        //     console.log(`User changed the value to ${myTimepicker.value}`);
            
        //     let [hours, minutes] = myTimepicker.value.split(":");
            
        //     minutes = (Math.ceil(minutes / 15) * 15);
        //     if (minutes == 0) minutes = "00";
        //     if (minutes == 60) { minutes = "00"; ++hours % 24; }
                
        //     let newValue = hours + ":" + minutes;
            
        //     console.log(`Rounding value to ${newValue}`);
            
        //     myTimepicker.value = newValue;
        // });
        // myTimepicker.timepicker({ 'step': 15 });

    })(jQuery);

</script>
<script>
    $(document).ready(function(){
        $('input.timepicker').timepicker({ 
            zindex: 9999999,
            timeFormat: 'HH:mm',
            interval: 45,
            minTime: '11',
            maxTime: '16',
            startTime: '11:00',
            dynamic: false,
            dropdown: true,
            scrollbar: false,
            change: tm
        });


        function tm(){
            
        //  $(document).on('change', '.picktime', function() {
            var picktime = $('input[name=picktime]').val();
            var pickdate = $('input[name=pickdate]').val();
            // console.log('s');
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('check.brodev')}}",
                data: {
                    'picktime': picktime,
                    'pickdate': pickdate,
                    '_token': token
                },
                success: function(data) {
                    // console.log();
                    if (data.success) {
                        // $('.position').removeAttr('disabled');
                        // $('.position-test').text('');
                        // // console.log(this.form.elements['ref_name'].value);\
                        // console.log('s');
                        $('input[name=pickdate]').val('');
                        $('input[name=picktime]').val('');
                        document.getElementById('pick-error2').style.display = 'block';
                    } else {
                        // console.log('ss');
                        document.getElementById('pick-error2').style.display = 'none';
                        // console.log('ss');
                        // $('.position').attr('disabled', true);
                        // $('.position-test').html(not_select_msg);
                    }
                    // $("#ref").html(data.msg);
                }
            });

        // });

        }

    });
</script>
{{-- <script>
    function createOption(value, text) {
           var option = document.createElement('option');
           option.text = text;
           option.value = value;
           return option;
    }

    var hourSelect = document.getElementById('hours');
    for(var i = 11; i <= 16; i++){
           hourSelect.add(createOption(i, i));
    }

    var minutesSelect = document.getElementById('minutes');
    for(var i = 0; i < 60; i += 15) {
           minutesSelect.add(createOption(i, i));
    }
</script> --}}
@endpush
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endpush