@extends($activeTemplate . 'user.layouts.app')

@section('panel')
<div class="row">

    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class="table-responsive--sm table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('SL')</th>
                                <th scope="col">@lang('Time')</th>
                                <th scope="col">@lang('Delivery ID')</th>
                                <th scope="col">@lang('Product')</th>
                                <th scope="col">@lang('BRO Package QTY')</th>
                                <th scope="col">@lang('Delivery ')</th>
                                <th scope="col">@lang('Status')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($delivery as $ex)
                            <tr>
                                <td data-label="@lang('SL')">{{ $delivery->firstItem()+$loop->index }}</td>
                                <td data-label="@lang('Date')">{{ showDateTime($ex->created_at) }}</td>
                                <td data-label="@lang('Delivery ID')">{{ $ex->trx }}</td>
                                <td data-label="@lang('Product')">
                                    @if ($ex->product == 0)
                                        {{-- <span class="badge badge--warning">@lang('Please select product first!!')</span> --}}
                                        <a href="{{route('user.plan.select.type',[$user->id,$ex->trx])}}" class="btn btn-primary">Select Product</a>
                                    @elseif($ex->product == 1)
                                    <button class="btn btn--primary btn-sm ml-1 detailBtnProd" data-id="{{ $ex->id }}"
                                            data-toggle="tooltip" data-original-title="@lang('Product Selected')"><i
                                            class="fas fa-info"></i>
                                        @lang('Product Selected')
                                    </button>
                                    {{-- @dump(brodetail($ex->id)) --}}
                                    @endif
                                </td>
                                <td data-label="@lang('BRO Package QTY')">{{ nb($ex->bro_qty) }}</td>
                                <td data-label="@lang('Delivery ')">
                                    @if ($ex->ship_method == 1)
                                    Pick Up Offline at {{ showDateTime($ex->pickupdate) }}
                                    @elseif ($ex->ship_method == 2)
                                    {{ Str::limit($ex->alamat,40) }}
                                    @else
                                    Converted to gold 
                                    @endif
                                </td>
                                <td data-label="@lang('Status')">
                                    @if ($ex->product == 0)
                                        <span class="badge badge--warning">@lang('Please select product first!!')</span>
                                    @else
                                        @if ($ex->ship_method == 1)
                                            @if($ex->status == 1)
                                            <span class="badge badge--success">@lang('Complete. Already Pick Up')</span>
                                            @elseif($ex->status == 2)
                                            <span class="badge badge--warning">@lang('Waiting for Pick Up')</span>
                                            @elseif($ex->status == 3)
                                            <span class="badge badge--danger">@lang('Cancel')</span>
                                            @endif

                                            @if($ex->no_resi != null)
                                            <button class="btn--info btn-rounded  badge detailBtn"
                                                data-no_resi="{{$ex->no_resi}}" data-sm="{{$ex->ship_method}}"><i
                                                    class="fa fa-info"></i></button>
                                            @endif
                                        @else
                                            @if($ex->status == 1)
                                            <span class="badge badge--success">@lang('Complete. On Delivery')</span>
                                            @elseif($ex->status == 2)
                                            <span class="badge badge--warning">@lang('Pending')</span>
                                            @elseif($ex->status == 3)
                                            <span class="badge badge--danger">@lang('Cancel')</span>
                                            @endif
                                            @if($ex->no_resi != null)
                                            <button class="btn--info btn-rounded  badge detailBtn"
                                                data-no_resi="{{$ex->no_resi}}" data-sm="{{$ex->ship_method}}"><i
                                                    class="fa fa-info"></i></button>
                                            @endif
                                        @endif
                                    @endif

                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $delivery->appends($_GET)->links() }}
            </div>
        </div>
    </div>
</div>

<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><span class="withdraw-detail2"></span> : <span class="withdraw-detail"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>

@forelse($delivery as $ex)
<div id="detailprod{{$ex->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details Product Selected')</h5>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table--light style--two">
                    <thead>
                        <tr>
                            <th scope="col">@lang('Product')</th>
                            <th scope="col" width="20%">@lang('Qty')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse(brodetail($ex->id) as $exd)
                            <tr>
                                <td data-label="@lang('Product')">
                                    {{cekproduk($exd->product)}}
                                </td>
                                <td data-label="@lang('Qty')">
                                    {{$exd->q}}
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection


{{-- @push('breadcrumb-plugins')
<form action="" method="GET" class="form-inline float-sm-right bg--white">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('Search by TRX')"
            value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
@endpush --}}

@push('script')
<script>
    $('.detailBtn').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('no_resi');
                var sm = $(this).data('sm');
                modal.find('.withdraw-detail').html(feedback);
                
                
                if (sm == 1) {
                    modal.find('.withdraw-detail2').html('Pick Up By');
                    
                }else{
                    modal.find('.withdraw-detail2').html('Reciept Number');
                    
                }

                modal.modal('show');
        });
</script>

<script>
    $('.detailBtnProd').on('click', function () {
                var id = $(this).data('id');
                var modal = $('#detailprod'+id);
                modal.modal('show');
    });
</script>
@endpush