@extends($activeTemplate . 'user.layouts.app')

@section('panel')
    <div class="row">

        
        
        @foreach($reward as $data)
        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-6 mb-30">
            <div class="card h-100">
                <div class="card-body pt-5">
                    <div class="pricing-table text-center mb-4">
                        <img class="img-fluid" src="{{ getImage('assets/images/reward/'. $data->rewa->images,  null, true)}}"
                            alt="">
                        <h4 class="package-name mb-20 text-"><strong>@lang($data->rewa->reward)</strong></h4>
                        <p>Ticket ID : {{$data->trx}}</p>
                    </div>
                    <div class="row px-10">
                        <a class="btn btn--success btn-block" href="{{ route('user.ticket.print', $data->trx) }}" target="_blank" class="print">Print Ticket <i class="las la-print"></i></a>
                    </div>
                </div>

            </div><!-- card end -->
        </div>
        @endforeach
    </div>

@endsection