

<div class="row align-items-center mb-30 justify-content-between">
    <div class="col-lg-6 col-sm-6">
        <h6 class="page-title">{{ __($page_title) }}</h6> 
        @if (isset($status_page))
            @if ($status_page == '1')
            <span class="badge badge-success">Active</span>
            @else
            <span class="badge badge-danger">Inactive</span>
            @endif
        @endif
    </div>
    <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
        @stack('breadcrumb-plugins')
    </div>
</div>
