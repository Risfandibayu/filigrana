@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body p-0 text-center">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Reward')</th>
                                <th scope="col">@lang('Type')</th>
                                <th scope="col">@lang('Harga Modal')</th>
                                <th scope="col">@lang('QTY')</th>
                                <th scope="col">@lang('Total')</th>
                                <th scope="col">@lang('Probability')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($gacha as $key => $item)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($item->name) }}</td>
                                <td data-label="@lang('Type')" class="text-capitalize">{{ __($item->type) }}</td>
                                <td data-label="@lang('Harga Modal')">Rp. {{nb($item->modal) }}</td>
                                <td data-label="@lang('QTY')">{{nb($item->qty) }}</td>
                                <td data-label="@lang('Total')">Rp. {{nb($item->qty * $item->modal) }}</td>
                                
                                <td data-label="@lang('Probability')">
                                    @if($total_qty != 0)
                                        {{ ($item->qty / $total_qty) * 100 }}%
                                    @else
                                        0%
                                    @endif
                                </td>

                                <td data-label="@lang('Action')">
                                    <a href="{{route('admin.gacha.user.plot',[$item->id])}}" type="button" class="icon-btn" data-toggle="tooltip"
                                        data-id="{{ $item->id }}">
                                        <i class="la la-pencil">User plot</i>
                                    </a>
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $item->id }}"
                                        data-name="{{ $item->name }}"
                                        data-modal="{{ $item->modal }}"
                                        data-type="{{ $item->type }}"
                                        data-qty="{{ $item->qty }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                            <tr>
                                <td colspan="4">Total</td>
                                <td>{{$total_qty}}</td>
                                <td>Rp {{nb($total)}}</td>
                                <td>100%</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4">BRO Last Week</td>
                                <td>{{broJoinedToday()}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4">Redeem Percent</td>
                                <td>{{$gachamaster->reward_percent}}%</td>
                                <td></td>
                                <td></td>
                                <td><button class="btn btn-primary edit-gacha" data-id="{{$gachamaster->id}}" data-reward_percent="{{$gachamaster->reward_percent}}">Update Redeem Percent</button></td>
                            </tr>
                            <tr>
                                <td colspan="4">Rule Accumulation <br>(BRO Last Week x Rp 750.000 x Redeem Percent)</td>
                                <td>{{broJoinedToday()}} x Rp. 750.000 x {{$gachamaster->reward_percent}}%</td>
                                <td>Rp. {{nb(broJoinedToday() * 750000 * ($gachamaster->reward_percent/100))}}</td>
                                <td></td>
                                <td>
                                    @if ($total > (broJoinedToday() * 750000 * ($gachamaster->reward_percent/100)))
                                        <button class="btn btn-primary" disabled>Activated Gacha</button>
                                        <br>
                                        <span class="text-danger">Total Melebihi Rule Redeem</span>
                                    @else
                                        <form method="post" action="{{route('admin.gacha.status',[$gachamaster->id])}}" enctype="multipart/form-data">
                                            @csrf
                                            <button type="submit" class="btn btn-primary">Activated Gacha</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $gacha->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>


<div id="edit-gacha" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Redeem Percent')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.gacha.update')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input class="form-control id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="font-weight-bold"> @lang('Gacha Percent') </label>
                            <div class="input-group">
                                <input type="text" class="form-control reward_percent" placeholder="10000" name="reward_percent" required>
                                <div class="input-group-prepend"><span class="input-group-text">%
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- edit modal--}}
<div id="edit-detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Gacha Detail')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.gacha.update.detail')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Harga Modal') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control modal2" placeholder="10000" name="modal" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('QTY')</label>
                            <input type="number" class="form-control qty" name="qty" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type Item')</label>
                            {{-- <input type="number" class="form-control qty" name="qty" required> --}}
                            <select name="type" id="type" class="form-control">
                                <option value="" hidden selected>-- Select Item Type --</option>
                                <option value="super rare">Super rare</option>
                                <option value="rare">Rare</option>
                                <option value="uncommon">Uncommon</option>
                                <option value="common">Common</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Prize Image') <small>(recommended image ratio 9:16)</small></label>
                            <input class="form-control form-control-lg image" type="file" accept="image/*"  onchange="loadFile(event)" name="images">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="add-detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Gacha Detail')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.gacha.add.detail')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control id" type="hidden" name="gacha_id" value="{{$gachamaster->id}}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Harga Modal') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control modal2" placeholder="10000" name="modal" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('QTY')</label>
                            <input type="number" class="form-control qty" name="qty" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type Item')</label>
                            {{-- <input type="number" class="form-control qty" name="qty" required> --}}
                            <select name="type" id="type" class="form-control">
                                <option value="" hidden selected>-- Select Item Type --</option>
                                <option value="super rare">Super rare</option>
                                <option value="rare">Rare</option>
                                <option value="uncommon">Uncommon</option>
                                <option value="common">Common</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Prize Image') <small>(recommended image ratio 9:16)</small></label>
                            <input class="form-control form-control-lg image" type="file" accept="image/*"  onchange="loadFile(event)" name="images">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="edit-product-stok" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Update Product Stock')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.products.updatestok')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="names"  placeholder="Product ABC" readonly>
                        </div>
                        
                    </div>

                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type')</label>
                            <input type="number" class="form-control weight" name="weight" step="0.001" placeholder="0.001" required>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Stock')</label>
                            <input type="number" class="form-control stok" name="stok" step="0" placeholder="0" >
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" class="t1" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" id="status" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Pricing For')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Reseller')" data-off="@lang('Public')"
                                name="reseller" checked>
                        </div>
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="add-product" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Product')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('admin.products.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control plan_id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Product Image') <small>(recommended image ratio 9:16)</small></label>
                            <input class="form-control form-control-lg" type="file" accept="image/*"  onchange="loadFile(event)" name="images"  required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Price') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control" placeholder="10000" name="price" required>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Type')</label>
                            <input type="number" class="form-control" name="weight" step="0.001" placeholder="0.001" required>
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Stock')</label>
                            <input type="number" class="form-control" name="stok" step="0" placeholder="0" >
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Pricing For')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Reseller')" data-off="@lang('Public')"
                                name="reseller" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

{{-- @push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-product"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush --}}

@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-detail"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush
@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                var modal = $('#edit-detail');
                modal.find('.name').val($(this).data('name'));
                modal.find('.modal2').val($(this).data('modal'));
                modal.find('.qty').val($(this).data('qty'));

                modal.find('select[name=type]').val($(this).data('type'));
              
                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.edit-gacha').on('click', function () {
                var modal = $('#edit-gacha');
                modal.find('.reward_percent').val($(this).data('reward_percent'));

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-detail').on('click', function () {
                var modal = $('#add-detail');
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                var modal = $('#edit-product-stok');
                modal.find('.name').val($(this).data('name'));
                modal.find('.stok').val($(this).data('stok'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush