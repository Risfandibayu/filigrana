@extends('admin.layouts.app')

@section('panel')
<div class="row">
    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class=" table-responsive p-2" >
                <input hidden type="text" id="min2" name="min2">
                <input hidden type="text" id="max2" name="max2">
                    <table class="table table--light style--two" id="myTable" >
                        <thead>
                            <tr>
                                <th >@lang('Date')</th>
                                <th >@lang('Time')</th>
                                <th >@lang('Username')</th>
                                <th >@lang('Prize')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($gacha as $trx)
                            <tr>
                                <td >{{ date('Y-m-d',strtotime($trx->created_at)) }}</td>
                                <td >{{ date('H:i:s',strtotime($trx->created_at)) }}</td>
                                <td class="font-weight-bold">
                                    <a
                                        href="{{ route('admin.users.detail', $trx->user_id) }}">{{ cekuser($trx->user_id)
                                        }}</a>
                                </td>
                                <td  class="budget">{{ cekprize($trx->gachamasterdetail_id) }}</td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            {{-- <div class="card-footer py-4">
                {{ $transactions->links('admin.partials.paginate') }}
            </div> --}}
        </div><!-- card end -->
    </div>
</div>
@endsection


@push('breadcrumb-plugins')
{{-- @if (request()->routeIs('admin.users.transactions'))
<form action="" method="GET" class="form-inline float-sm-right bg--white">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('TRX / Username')"
            value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
@else
<form action="{{ route('admin.report.transaction.search') }}" method="GET" class="form-inline float-sm-right bg--white">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('TRX / Username')"
            value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>
@endif --}}
{{-- <div class="row">

    <div class="col-md-10 col-12">
        <form action="{{ route('admin.report.transaction') }}" method="GET" class="form-inline float-sm-right bg--white ml-2">

            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Username/BRO No')"
                    value="{{ $search ?? '' }}">
                <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en"
                    class="datepicker-here bg--white text--black form-control" data-position='bottom right'
                    placeholder="@lang('Min - Max date')" autocomplete="off" value="{{ @$dates }}" readonly>
                <input hidden type="text" name="page" class="form-control" placeholder="@lang('Username or email')"
                    value="{{ $page_title ?? '' }}">
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-2 col-4">
        <form action="{{ route('admin.report.export') }}" method="GET" class="form-inline float-sm-right">
            <input hidden type="text" name="search" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $search ?? '' }}">
            <input hidden type="text" name="date" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $dates ?? '' }}">
            <input hidden type="text" name="page" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $page_title ?? '' }}">
            <button class="btn btn--primary" type="submit">Export</button>
        </form>
    </div>
</div> --}}
@endpush
@push('script')
<script src="{{ asset('assets/admin/js/vendor/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/vendor/datepicker.en.js') }}"></script>
<script>
    'use strict';
  (function($){
      if(!$('.datepicker-here').val()){
          $('.datepicker-here').datepicker();
      }
  })(jQuery)
</script>

  
<script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>
<script>
    $(document).ready( function () {
    // $('#myTable').DataTable({
    //     "pageLength": 25,
    //     order: [[0, 'desc']],
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'copy', 'csv', 'excel'
    //     ]
    // });
} );
</script>



<script>
    
    let minDate, maxDate;
 
 // Custom filtering function which will search data in column four between two values
 DataTable.ext.search.push(function (settings, data, dataIndex) {
     let min = minDate.val();
     let max = maxDate.val();
     let date = new Date(data[0]);
  
     if (
         (min === null && max === null) ||
         (min === null && date <= max) ||
         (min <= date && max === null) ||
         (min <= date && date <= max)
     ) {
         return true;
     }
     return false;
 });
  
 // Create date inputs
 minDate = new DateTime('#min2', {
     format: 'YYYY-MM-DD'
 });
 maxDate = new DateTime('#max2', {
     format: 'YYYY-MM-DD'
 });
  
 // DataTables initialisation
 let table = $('#myTable').DataTable({
        "pageLength": 25,
        order: [[0, 'desc']],
        responsive: true,
        // sDom: "Bfrtip",
        // sDom: '<"top"Bf>rt<"bottom"ip><"clear">',
        sDom: "<'row'<'col-sm-3'B><'col-sm-6'<'toolbar row'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'lp>>",
                fnInitComplete: function(){
                    $('div.toolbar').html('<div class="col-md-3" style="font-size: 12px;line-height: 3;">Search By Date : </div> <div class="col-md-4"><input class="form-control"  type="date" id="min" name="min"></div><div class="col-md-4"><input class="form-control"  type="date" id="max" name="max"></div>');
                },
        buttons: [
            'copy', 'csv', 'excel'
        ]
    });
    // $('div.toolbar').html('');
    $('#min').change(function() {
        var date = $(this).val();
        $('#min2').val($(this).val());
        minDate.val($(this).val());
        // console.log(minDate.val());
    });
    $('#max').change(function() {
        var date = $(this).val();
        $('#max2').val($(this).val());
        maxDate.val($(this).val());
        // console.log(maxDate.val());
    });
  
 // Refilter the table
 document.querySelectorAll('#min2, #max2').forEach((el) => {
     el.addEventListener('change', () => table.draw());
 });
</script>

@endpush

@push('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">
@endpush