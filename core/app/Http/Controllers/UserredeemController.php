<?php

namespace App\Http\Controllers;

use App\Models\userredeem;
use App\Http\Controllers\Controller;
use App\Models\redeemmaster;
use App\Models\redeemmasterdetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserredeemController extends Controller
{
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function saka_vaganza(){
        $kuota = Auth::user()->gacha_qty;
        $items = redeemmasterdetail::all();
        return view($this->activeTemplate .'user.redeem.index',compact('items','kuota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\userredeem  $userredeem
     * @return \Illuminate\Http\Response
     */
    public function show(userredeem $userredeem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\userredeem  $userredeem
     * @return \Illuminate\Http\Response
     */
    public function edit(userredeem $userredeem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\userredeem  $userredeem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, userredeem $userredeem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\userredeem  $userredeem
     * @return \Illuminate\Http\Response
     */
    public function destroy(userredeem $userredeem)
    {
        //
    }

    public function claim(Request $request) {
        $selectedItem = $request->item;
    
        // Save the gacha result and decrement the item quantity atomically
        $successful = false;
        $userId = Auth::user()->id;
        $item = null;
    
        DB::transaction(function () use ($userId, $selectedItem, &$successful, &$item) {
            // Reload the selected item for accurate decrement within a transaction
            $item = redeemmasterdetail::find($selectedItem);
    
            if ($item && $item->qty > 0) {
                userredeem::create([
                    'user_id' => $userId,
                    'item_id' => $item->id,
                ]);
                $item->decrement('qty');
                $user = User::find($userId);
                $user->decrement('gacha_qty');
    
                $successful = true;
            }
        });
    
        if ($successful) {
            return response()->json([
                'status' => 200,
                'item' => $item,
                'message' => 'Berhasil Di Klaim',
                'type' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal mengklaim item. Stok mungkin habis atau ada masalah lainnya.',
                'type' => 'error'
            ]);
        }
    }
    
}
