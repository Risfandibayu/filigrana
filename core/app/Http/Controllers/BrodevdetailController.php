<?php

namespace App\Http\Controllers;

use App\Models\Brodevdetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BrodevdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brodevdetail  $brodevdetail
     * @return \Illuminate\Http\Response
     */
    public function show(Brodevdetail $brodevdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brodevdetail  $brodevdetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Brodevdetail $brodevdetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brodevdetail  $brodevdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brodevdetail $brodevdetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brodevdetail  $brodevdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brodevdetail $brodevdetail)
    {
        //
    }
}
