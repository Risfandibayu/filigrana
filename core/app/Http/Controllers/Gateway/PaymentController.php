<?php

namespace App\Http\Controllers\Gateway;

use App\Http\Controllers\Controller;
use App\Models\AdminNotification;
use App\Models\Deposit;
use App\Models\DirectPlan;
use App\Models\GatewayCurrency;
use App\Models\GeneralSetting;
use App\Models\Plan;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use Xendit\Xendit;

class PaymentController extends Controller
{
    public function __construct()
    {
        return $this->activeTemplate = activeTemplate();
    }

    public function deposit()
    {
        $gatewayCurrency = GatewayCurrency::whereHas('method', function ($gate) {
            $gate->where('status', 1);
        })->with('method')->orderby('method_code')->get();
        $page_title = 'Deposit Methods';
        return view($this->activeTemplate . 'user.payment.deposit', compact('gatewayCurrency', 'page_title'));
    }

    public function depositInsert(Request $request)
    {
        $depo = Deposit::where('user_id',Auth::user()->id)->where('expired','>',Carbon::now()->toDateTimeString())->where('status','=',2)->first();

        // dd($depo);
        if ($depo) {
            # code...
            $notify[] = ['error', 'There is an unpaid deposit, please check your deposit history and make a payment'];
            return back()->withNotify($notify);
        }

        // dd($request->all());
        // $request->amount =  str_replace('.','',$request->amount) + rand(10, 99);
        $request->amount =  str_replace('.','',$request->amount);
        // dd($request->amount);

        $request->validate([
            'amount' => 'required',
            'method_code' => 'required',
            'currency' => 'required',
        ]);


        $user = auth()->user();
        $gate = GatewayCurrency::where('method_code', $request->method_code)->where('currency', $request->currency)->first();
        if (!$gate) {
            $notify[] = ['error', 'Invalid Gateway'];
            return back()->withNotify($notify);
        }

        // if ($gate->min_amount  > $request->amount || $gate->max_amount + rand(10, 99) < $request->amount) {
        if ($gate->min_amount  > $request->amount || $gate->max_amount < $request->amount) {
            $notify[] = ['error', 'Please Follow Deposit Limit'];
            return back()->withNotify($notify);
        }

        $charge = getAmount($gate->fixed_charge + ($request->amount * $gate->percent_charge / 100));
        $payable = getAmount($request->amount + $charge);
        $final_amo = getAmount($payable * $gate->rate);

        $data = new Deposit();
        $data->user_id = $user->id;
        $data->method_code = $gate->method_code;
        $data->method_currency = strtoupper($gate->currency);
        $data->amount = $request->amount;
        $data->charge = $charge;
        $data->rate = $gate->rate;
        $data->final_amo = getAmount($final_amo);
        $data->btc_amo = 0;
        $data->btc_wallet = "";
        $data->trx = getTrx();
        $data->try = 0;
        $data->status = 0;
        $data->save();
        session()->put('Track', $data['trx']);
        return redirect()->route('user.deposit.preview');
    }


    public function depositPreview()
    {

        $track = session()->get('Track');
        $data = Deposit::where('trx', $track)->orderBy('id', 'DESC')->firstOrFail();

        if (is_null($data)) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        if ($data->status != 0) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        $page_title = 'Payment Preview';

        // Ipaymu
        // $va           = env('IPAY_VA'); //get on iPaymu dashboard
        // $secret       = env('IPAY_SECRET'); //get on iPaymu dashboard

        // if (env('APP_ENV') != 'production') {
        //     # code...
        //     $url          = 'https://sandbox.ipaymu.com/api/v2/payment-method-list'; // for development mode
        // }else{
        //     $url          = 'https://my.ipaymu.com/api/v2/payment-method-list'; // for production mode
        // }
        
        // $method       = 'POST';

        // $body = '';

        //         $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
        //         $requestBody  = strtolower(hash('sha256', $jsonBody));
        //         $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
        //         $signature    = hash_hmac('sha256', $stringToSign, $secret);
        //         $timestamp    = Date('YmdHis');

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        // CURLOPT_URL => $url,
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => '',
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => true,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => 'POST',
        // CURLOPT_HTTPHEADER => array(
        //     'Content-Type: application/json',
        //     'signature: '.$signature,
        //     'va: '.$va,
        //     'timestamp: ' .$timestamp
        // ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // $res = json_decode($response);
        // // dd($res);
        // // dd($res->Data[0]->Channels);
        // $paymet = $res->Data[0]->Channels;



        // xendit
        Xendit::setApiKey(config('xendit.key_auth'));

        $paymet = \Xendit\VirtualAccounts::getVABanks();
        // var_dump($getVABanks);
        // dd($paymet);


        return view($this->activeTemplate . 'user.payment.preview', compact('data','paymet', 'page_title'));
    }


    public function depositConfirm()
    {
        $track = Session::get('Track');
        $deposit = Deposit::where('trx', $track)->orderBy('id', 'DESC')->with('gateway')->first();
        if (is_null($deposit)) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        if ($deposit->status != 0) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }

        if ($deposit->method_code >= 1000) {
            $this->userDataUpdate($deposit);
            $notify[] = ['success', 'Your deposit request is queued for approval.'];
            return back()->withNotify($notify);
        }


        $dirName = $deposit->gateway->alias;
        $new = __NAMESPACE__ . '\\' . $dirName . '\\ProcessController';

        $data = $new::process($deposit);
        $data = json_decode($data);


        if (isset($data->error)) {
            $notify[] = ['error', $data->message];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        if (isset($data->redirect)) {
            return redirect($data->redirect_url);
        }

        // for Stripe V3
        if(@$data->session){
            $deposit->btc_wallet = $data->session->id;
            $deposit->save();
        }

        $page_title = 'Payment Confirm';
        return view($this->activeTemplate . $data->view, compact('data', 'page_title', 'deposit'));
    }


    public static function userDataUpdate($trx)
    {
        $gnl = GeneralSetting::first();
        $data = Deposit::where('trx', $trx)->first();
        if ($data->status == 0) {
            $data->status = 1;
            $data->save();

            $user = User::find($data->user_id);
            $user->balance += $data->amount;
            // $user->gacha_qty += $data->qty;
            $user->save();
            $gateway = $data->gateway;

            $transaction = new Transaction();
            $transaction->user_id = $data->user_id;
            $transaction->amount = $data->amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = getAmount($data->charge);
            $transaction->trx_type = '+';
            $transaction->details = 'Deposit Via ' . $data->gateway_currency()->name;
            $transaction->trx = $data->trx;
            $transaction->save();

            $adminNotification = new AdminNotification();
            $adminNotification->user_id = $user->id;
            $adminNotification->title = 'Deposit successful via '.$data->gateway_currency()->name;
            $adminNotification->click_url = route('admin.deposit.successful');
            $adminNotification->save();

            notify($user, 'DEPOSIT_COMPLETE', [
                'method_name' => $data->gateway_currency()->name,
                'method_currency' => $data->method_currency,
                'method_amount' => getAmount($data->final_amo),
                'amount' => getAmount($data->amount),
                'charge' => getAmount($data->charge),
                'currency' => $gnl->cur_text,
                'rate' => getAmount($data->rate),
                'trx' => $data->trx,
                'post_balance' => getAmount($user->balance)
            ]);


        }
    }

    public static function userDataUpdate2($trx)
    {
        $gnl = GeneralSetting::first();
        $data = Deposit::where('trx', $trx)->first();
        if ($data->status == 2) {
            $data->status = 1;
            $data->save();

            $user = User::find($data->user_id);
            $user->balance += $data->amount;
            // $user->gacha_qty += $data->qty;
            $user->save();
            $gateway = $data->gateway;

            $transaction = new Transaction();
            $transaction->user_id = $data->user_id;
            $transaction->amount = $data->amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = getAmount($data->charge);
            $transaction->trx_type = '+';
            $transaction->details = 'Deposit Via ' . $data->gateway_currency()->name;
            $transaction->trx = $data->trx;
            $transaction->save();

            $adminNotification = new AdminNotification();
            $adminNotification->user_id = $user->id;
            $adminNotification->title = 'Deposit successful via '.$data->gateway_currency()->name;
            $adminNotification->click_url = route('admin.deposit.successful');
            $adminNotification->save();

            notify($user, 'DEPOSIT_COMPLETE', [
                'method_name' => $data->gateway_currency()->name,
                'method_currency' => $data->method_currency,
                'method_amount' => getAmount($data->final_amo),
                'amount' => getAmount($data->amount),
                'charge' => getAmount($data->charge),
                'currency' => $gnl->cur_text,
                'rate' => getAmount($data->rate),
                'trx' => $data->trx,
                'post_balance' => getAmount($user->balance)
            ]);


        }
    }


    //Redirect Payment
    // public function manualDepositConfirm(Request $request)
    // {
    //     // dd($request->all());
    //     $track = session()->get('Track');
    //     $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
    //     if (!$data) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     if ($data->status != 0) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     // dd($data);

    //     $user = user::where('id',$data->user_id)->first();

    //     $va           = env('IPAY_VA'); //get on iPaymu dashboard
    //     $secret       = env('IPAY_SECRET'); //get on iPaymu dashboard

    //     if (env('APP_ENV') != 'production') {
    //         # code...
    //         $url          = 'https://sandbox.ipaymu.com/api/v2/payment'; // for development mode
    //     }else{
    //         $url          = 'https://my.ipaymu.com/api/v2/payment'; // for production mode
    //     }
        
    //     $method       = 'POST'; //method
        
    //     //Request Body//
    //     $body['product']    = array('Deposit');
    //     $body['qty']        = array('1');
    //     $body['price']      = array($data->final_amo);
    //     $body['buyerName']  = $user->firstname . ' '. $user->lastname;
    //     $body['buyerEmail'] = $user->email;
    //     $body['buyerPhone'] = $user->mobile;
    //     // $body['expired'] = 10;
    //     // $body['expiredType'] = 'seconds';
    //     // $body['autoRedirect'] = 10;
    //     // $body['returnUrl']  = env('APP_URL').'/thank-you';
    //     $body['returnUrl']  = route('user.home');
    //     $body['cancelUrl']  = env('APP_URL').'/cancel-payment';
    //     $body['notifyUrl']  = env('APP_URL').'/callback-url';
    //     $body['referenceId'] = $data->trx; //your reference id
    //     //End Request Body//

    //     //Generate Signature
    //     // *Don't change this
    //     $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
    //     $requestBody  = strtolower(hash('sha256', $jsonBody));
    //     $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
    //     $signature    = hash_hmac('sha256', $stringToSign, $secret);
    //     $timestamp    = Date('YmdHis');
    //     //End Generate Signature


    //     $ch = curl_init($url);

    //     $headers = array(
    //         'Accept: application/json',
    //         'Content-Type: application/json',
    //         'va: ' . $va,
    //         'signature: ' . $signature,
    //         'timestamp: ' . $timestamp
    //     );

    //     curl_setopt($ch, CURLOPT_HEADER, false);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     curl_setopt($ch, CURLOPT_POST, count($body));
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //     $err = curl_error($ch);
    //     $ret = curl_exec($ch);
    //     curl_close($ch);

    //     // dd($body);

    //     if($err) {
    //         echo $err;
    //     } else {

    //         //Response
    //         $ret = json_decode($ret);
    //         if($ret->Status == 200) {
    //             $sessionId  = $ret->Data->SessionID;
    //             $url        =  $ret->Data->Url;
    //             // header('Location:' . $url);
    //             // dd($url);
    //             $data->status = 2;
    //             $data->url = $url;
    //             $data->save(); 
    //             return redirect()->to($url);
    //         } else {
    //             // echo $ret;
    //             $notify[] = ['error', 'Invalid deposit, please try again.'];
    //             return back()->withNotify($notify);
    //         }
    //         //End Response
    //     }
    // }


    // public function manualDepositConfirm(Request $request)
    // {
    //     // dd($request->all());
    //     $track = session()->get('Track');
    //     $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
    //     if (!$data) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     if ($data->status != 0) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     // dd($data);

    //     $user = user::where('id',$data->user_id)->first();

    //     $va           = env('IPAY_VA'); //get on iPaymu dashboard
    //     $secret       = env('IPAY_SECRET'); //get on iPaymu dashboard

    //     if (env('APP_ENV') != 'production') {
    //         # code...
    //         $url          = 'https://sandbox.ipaymu.com/api/v2/payment/direct'; // for development mode
    //     }else{
    //         $url          = 'https://my.ipaymu.com/api/v2/payment/direct'; // for production mode
    //     }
        
    //     $method       = 'POST'; //method
        
    //     //Request Body//
    //     $body['product']    = array('Deposit');
    //     $body['qty']        = array('1');
    //     $body['amount']      = intval($data->final_amo);
    //     $body['name']  = $user->firstname . ' '. $user->lastname;
    //     $body['email'] = $user->email;
    //     $body['phone'] = $user->mobile;
    //     $body['paymentMethod'] = 'va';
    //     $body['paymentChannel'] = $request->va;
    //     $body['expired'] = 7;
    //     $body['expiredType'] = 'days';
    //     // $body['autoRedirect'] = 10;
    //     // $body['returnUrl']  = env('APP_URL').'/thank-you';
    //     // $body['returnUrl']  = route('user.home');
    //     // $body['cancelUrl']  = env('APP_URL').'/cancel-payment';
    //     $body['notifyUrl']  = env('APP_URL').'/callback-url';
    //     $body['referenceId'] = $data->trx; //your reference id
    //     //End Request Body//

    //     //Generate Signature
    //     // *Don't change this
    //     $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
    //     $requestBody  = strtolower(hash('sha256', $jsonBody));
    //     $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $secret;
    //     $signature    = hash_hmac('sha256', $stringToSign, $secret);
    //     $timestamp    = Date('YmdHis');
    //     //End Generate Signature


    //     $ch = curl_init($url);

    //     $headers = array(
    //         'Accept: application/json',
    //         'Content-Type: application/json',
    //         'va: ' . $va,
    //         'signature: ' . $signature,
    //         'timestamp: ' . $timestamp
    //     );

    //     curl_setopt($ch, CURLOPT_HEADER, false);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //     curl_setopt($ch, CURLOPT_POST, count($body));
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);

    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //     $err = curl_error($ch);
    //     $ret = curl_exec($ch);
    //     curl_close($ch);
    //     // dd($ret);


    //     // dd($headers);

    //     if($err) {
    //         echo $err;
    //     } else {

    //         //Response
    //         $ret = json_decode($ret);
    //         if($ret->Status == 200) {
    //             // dd($ret);
    //             // $sessionId  = $ret->Data->SessionID;
    //             // $url        = $ret->Data->Url;
    //             // header('Location:' . $url);
    //             // dd($url);
    //             $data->status = 2;
    //             $data->trx_id = $ret->Data->TransactionId;
    //             $data->bank = $ret->Data->Channel;
    //             $data->payment = $ret->Data->PaymentNo;
    //             $data->payment_name = $ret->Data->PaymentName;
    //             $data->total = $ret->Data->Total;
    //             $data->fee = $ret->Data->Fee;
    //             $data->expired = $ret->Data->Expired;
    //             $data->save(); 

    //             return redirect()->route('user.deposit.payment', $data->trx);
    //         } else {
    //             // echo $ret;
    //             $notify[] = ['error', $ret->Message];
    //             return back()->withNotify($notify);
    //         }
    //         // End Response
    //         // dd($ret);
    //     }

    // }


    //redirect xendit
    public function manualDepositConfirmXendit(Request $request)
    {
        // dd($request->all());
        $track = session()->get('Track');
        $data = Deposit::where('status', 0)->where('trx', $track)->first();
        if (!$data) {
            return redirect()->route(gatewayRedirectUrl());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrl());
        }
        // dd($data);

        $user = user::where('id',$data->user_id)->first();

                // $data->status = 2;
                // $data->trx_id = $ret->Data->TransactionId;
                // $data->bank = $ret->Data->Channel;
                // $data->payment = $ret->Data->PaymentNo;
                // $data->payment_name = $ret->Data->PaymentName;
                // $data->total = $ret->Data->Total;
                // $data->fee = $ret->Data->Fee;
                // $data->expired = $ret->Data->Expired;
                // $data->save(); 

                Xendit::setApiKey(config('xendit.key_auth'));

                $params = ['external_id' => (string) "depo-".$data->trx,
                // 'payer_email' => $user->email,
                'description' => 'Deposit',
                'amount' => intval($data->final_amo),
                'invoice_duration' => 604800,
                'customer' => [
                    'given_names' => $user->firstname,
                    'surname' => $user->lastname,
                    'email' => $user->email,
                    'mobile_number' => $user->mobile,
                ],
                'success_redirect_url' => env('APP_URL').'/success/'.$data->id,
                'failure_redirect_url' => env('APP_URL').'/failed/'.$data->id,
                ];        

                $createInvoice = \Xendit\Invoice::create($params);
                // dd($createInvoice);


                $data->status = 2;
                $data->url = $createInvoice['invoice_url'];
                $data->expired = $createInvoice['expiry_date'];
                $data->save(); 
                return redirect()->to($createInvoice['invoice_url']);


        // return redirect()->route('user.deposit.payment', $data->trx);

    }
    public function manualDepositConfirm()
    {
        date_default_timezone_set('Asia/Jakarta'); // Set zona waktu sesuai dengan kebutuhan Anda

        list($microseconds, $seconds) = explode(' ', microtime());
        $milliseconds = sprintf('%03d', round($microseconds * 1000));
        $formatted_date = date('Y-m-d\TH:i:s.', $seconds) . $milliseconds . date('P', $seconds);

        $track = session()->get('Track');
        $data = Deposit::where('status', 0)->where('trx', $track)->first();
        if (!$data) {
            return redirect()->route(gatewayRedirectUrl());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrl());
        }

        $user = user::where('id',$data->user_id)->first();

        // Contoh data
        $httpMethod = "POST";
        $endpointUrl = "/payment/v2/h5/createLink";
        $timestamp = $formatted_date; // Misalnya timestamp dari header
        $mid = env('MID');
        $trxid = $data->id;
        $body = array(
            "merchantId" => $mid,
            "merchantTradeNo" => 'depo-'.$data->trx,
            "requestId" => $trxid,
            "amount" => intval($data->final_amo),
            "productName" => "Deposit",
            "payer" => $user->username,
            "phoneNumber" => $user->mobile,
            "notifyUrl" => route('deposit.manual.callback2'),
            "redirectUrl" => route(gatewayRedirectUrl()),
        ); // Contoh body JSON
        $privateKeyPath = base_path('private-key.pem');
        $privateKey = file_get_contents($privateKeyPath);

        // Langkah-langkah:
        // 1. Minify body JSON
        $minifiedBody = minifyJsonBody(json_encode($body));

        // 2. Membuat stringContent
        $stringContent = createStringContent($httpMethod, $endpointUrl, $minifiedBody, $timestamp);

        // 3. Membuat signature
        $signature = createSignature($stringContent, $privateKey);

        // Hasil signature



        $data_string = json_encode($body);

        // URL tujuan permintaan
        // $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl;
        if (env('APP_ENV') != 'production') {
            $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl; // for development mode
        }else{
            $url = 'https://pay.paylabs.co.id'.$endpointUrl; // for production mode
        }

        // Konfigurasi cURL
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=utf-8',
            'X-TIMESTAMP: '.$timestamp,
            'X-SIGNATURE: '.$signature,
            'X-PARTNER-ID: '.$mid,
            'X-REQUEST-ID: '.$trxid
            )
        );

        // Eksekusi permintaan dan ambil respons
        $result = curl_exec($ch);

        // Cek jika terjadi kesalahan
        if(curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        // Tutup koneksi cURL
        curl_close($ch);

        $response = json_decode($result,true);

        // dd($response);

        // echo "timestamp: ". $formatted_date."<br><br>";
        // echo "url : ".$url."<br><br>";
        // echo "body : ".$minifiedBody."<br><br>";
        // echo "stringContent : ".$stringContent."<br><br>";
        // echo 'X-TIMESTAMP : '.$timestamp."<br>";
        // echo "X-SIGNATURE : $signature"."<br>";
        // echo 'X-PARTNER-ID : '.$mid."<br>";
        // echo 'X-REQUEST-ID : '.$trxid."<br><br>";
        // echo "response : ".$result;
        // dd(json_decode($result,true));
        if ($response['errCode'] == "0") {
            # code...
            // $time = $response['expiredTime'];

            // // Memecah data waktu menjadi bagian-bagian yang relevan
            // $year = substr($time, 0, 4);
            // $month = substr($time, 4, 2);
            // $day = substr($time, 6, 2);
            // $hour = substr($time, 8, 2);
            // $minute = substr($time, 10, 2);
            // $second = substr($time, 12, 2);

            // // Format data waktu ke dalam format yang sesuai dengan tipe kolom datetime (YYYY-MM-DD HH:MM:SS)
            // $expireTime = "$year-$month-$day $hour:$minute:$second";

            $data->status = 2;
            // $data->paylabs_exptime = $expireTime;
            // $data->paylabs_platformtradeno = $response['platformTradeNo'];
            // $data->paylabs_productname = $response['productName'];
            // $data->paylabs_va = $response['productName'];
            $data->paylabs_url = $response['url'];
            $data->save(); 
            return redirect()->to($data->paylabs_url);
            // dd($response);
            // return redirect()->route('user.deposit.payment', $data->trx);

        }else{
            $notify[] = ['error', 'Error '.$response['errCode']];
            return redirect()->route('user.deposit')->withNotify($notify);
        }

    }

    public function payment($trx){
        $depo = deposit::where('trx','=',$trx)->first();
        if (!$depo) {
            # code...
            $notify[] = ['error', 'Invalid deposit ID, please try again.'];
            return back()->withNotify($notify);
        }
        if ($depo->user_id != Auth::user()->id) {
            # code...
            $notify[] = ['error', 'Invalid deposit ID, please try again.'];
            return back()->withNotify($notify);
        }
        $page_title = 'Payment Page';
        // $method = $data->gateway_currency();
        return view($this->activeTemplate . 'user.payment.payment', compact('depo','page_title'));
    }


    public function thankyou(Request $request){
        $page_title = 'Terima kasih telah melakukan Deposit';
        // $method = $data->gateway_currency();
        return view($this->activeTemplate . 'user.manual_payment.thankyou', compact( 'page_title'));
    }
    public function cancelpayment(Request $request){
        $page_title = 'Deposit Canceled';
        // $method = $data->gateway_currency();
        return view($this->activeTemplate . 'user.manual_payment.thankyou', compact( 'page_title'));
    }
    public function failpayment(Request $request){
        $page_title = 'Deposit Expired';
        // $method = $data->gateway_currency();
        return view($this->activeTemplate . 'user.manual_payment.failed', compact( 'page_title'));
    }
    public function callback(Request $request){
        
        $status = $request->status;
        $data = Deposit::where('trx', $request->reference_id)->first();
        if ($status == 'berhasil') {
            # code...
            $this->userDataUpdate3($data->trx);
            return response()->json(['status'=> $status]);
        }elseif($status == 'pending'){
            
            return response()->json(['status'=> $status]);
        }
        // }else{
        //     $data->status = 3;
        //     $data->admin_feedback =  "Deposit Canceled";
        //     $data->save();

        //     return response()->json(['status'=> $status]);
        // }
    }
    // public function callback2(Request $request){
        
    //     $status = $request->status;
    //     $data = Deposit::where('id', $request->external_id)->first();
    //     if ($data) {
    //         # code...
    //         if ($status == 'PAID') {
    //             # code...
    //             $this->userDataUpdate2($data->trx);
    //             return response()->json(['status'=> $status]);
    //         }elseif($status == 'EXPIRED'){
    //             $data->status = 3;
    //             $data->admin_feedback =  "Deposit Expired";
    //             $data->save();
    //             return response()->json(['status'=> $status]);
    //         }
    //     }else{
    //         return response()->json(['error'=> 'Data Not Found']);
    //     }



    //     // }else{
    //     //     $data->status = 3;
    //     //     $data->admin_feedback =  "Deposit Canceled";
    //     //     $data->save();

    //     //     return response()->json(['status'=> $status]);
    //     // }
    public function callback2Xendit(Request $request){
        
        $status = $request->status;
        $external_id = $request->external_id;

        if (strpos($external_id, 'plan-') === 0) {
            $trx = substr($external_id, strlen('plan-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = DirectPlan::where('trx', $trx)->first();
            
            if ($data) {
                if ($status == 'PAID') {
                    $this->planSuccess($data->trx);
                    return response()->json(['status' => $status]);
                } elseif ($status == 'EXPIRED') {
                    $data->status = 3;
                    $data->admin_feedback = "Payment Expired";
                    $data->save();
                    return response()->json(['status' => $status]);
                }
            } else {
                return response()->json(['error' => 'Data Not Found']);
            }

        } elseif (strpos($external_id, 'depo-') === 0) {
            $trx = substr($external_id, strlen('depo-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = Deposit::where('trx', $trx)->first();
            if ($data) {
                # code...
                if ($status == 'PAID') {
                # code...
                $this->userDataUpdate2($data->trx);
                return response()->json(['status'=> $status]);
                }elseif($status == 'EXPIRED'){
                    $data->status = 3;
                    $data->admin_feedback =  "Deposit Expired";
                    $data->save();
                    return response()->json(['status'=> $status]);
                }
            }else{
                return response()->json(['error'=> 'Data Not Found']);
            }

        } elseif (strpos($external_id, 'ro-') === 0) {
            $trx = substr($external_id, strlen('ro-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = DirectPlan::where('trx', $trx)->first();
            
            if ($data) {
                if ($status == 'PAID') {
                    $this->roSuccess($data->trx);
                    return response()->json(['status' => $status]);
                } elseif ($status == 'EXPIRED') {
                    $data->status = 3;
                    $data->admin_feedback = "Deposit Expired";
                    $data->save();
                    return response()->json(['status' => $status]);
                }
            } else {
                return response()->json(['error' => 'Data Not Found']);
            }

        } else {
            return response()->json(['error' => 'Invalid external_id format']);
        }



        // }else{
        //     $data->status = 3;
        //     $data->admin_feedback =  "Deposit Canceled";
        //     $data->save();

        //     return response()->json(['status'=> $status]);
        // }
    }
    public function callback2(Request $request){
        
        $status = $request->status;
        $external_id = $request->merchantTradeNo;

        if (strpos($external_id, 'plan-') === 0) {
            $trx = substr($external_id, strlen('plan-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = DirectPlan::where('trx', $trx)->first();
            
            if ($data) {
                if ($status == '02') {
                    $this->planSuccess($data->trx);
                    return response()->json(['status' => $status]);
                } else {
                    $data->status = 3;
                    $data->admin_feedback = "Payment Expired";
                    $data->save();
                    return response()->json(['status' => $status]);
                }
            } else {
                return response()->json(['error' => 'Data Not Found']);
            }

        } elseif (strpos($external_id, 'depo-') === 0) {
            $trx = substr($external_id, strlen('depo-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = Deposit::where('trx', $trx)->first();
            if ($data) {
                # code...
                if ($status == '02') {
                # code...
                $this->userDataUpdate2($data->trx);
                return response()->json(['status'=> $status]);
                }else{
                    $data->status = 3;
                    $data->admin_feedback =  "Deposit Expired";
                    $data->save();
                    return response()->json(['status'=> $status]);
                }
            }else{
                return response()->json(['error'=> 'Data Not Found']);
            }

        } elseif (strpos($external_id, 'ro-') === 0) {
            $trx = substr($external_id, strlen('ro-'));
            // Retrieve the DirectPlan record using the extracted trx ID
            $data = DirectPlan::where('trx', $trx)->first();
            
            if ($data) {
                if ($status == '02') {
                    $this->roSuccess($data->trx);
                    return response()->json(['status' => $status]);
                } else{
                    $data->status = 3;
                    $data->admin_feedback = "Deposit Expired";
                    $data->save();
                    return response()->json(['status' => $status]);
                }
            } else {
                return response()->json(['error' => 'Data Not Found']);
            }

        } else {
            return response()->json(['error' => 'Invalid external_id format']);
        }



        // }else{
        //     $data->status = 3;
        //     $data->admin_feedback =  "Deposit Canceled";
        //     $data->save();

        //     return response()->json(['status'=> $status]);
        // }
    }

    public function planSuccess($trx)
    {
        $gnl = GeneralSetting::first();
        $data = DirectPlan::where('trx', $trx)->first();
        if ($data->status == 2) {
            
            $data->status = 1;
            $data->save();

            $plan = Plan::where('id', $data->plan_id)->first();

            $user = user::where('id', $data->user_id)->first();
            $ref_user = User::where('no_bro', $data->referral)->first();
            $up_user = User::where('no_bro', $data->upline)->first();        

            $oldPlan = $user->plan_id;
            
            $pos = getPosition($ref_user->id, $data->position);
            $user->no_bro       = generateUniqueNoBro();
            $user->ref_id= $ref_user->id;
            // $user->pos_id= $pos['pos_id'];
            // $user->pos_id= $up_user->id;
            $user->pos_id= isset($up_user->id) ? $up_user->id : $pos['pos_id'];
            $user->position= $data->position;
            $user->plan_id = $plan->id;
            // $user->balance -= ($plan->price * $data->qty);
            $user->total_invest += ($plan->price * $data->qty);
            $user->bro_qty += ($data->qty - 1);
            $user->is_matriks = $data->qty>14 ? 1 : 0;
            $user->gacha_qty += $data->qty;
            $user->save();

            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {

                if ($data->shipmethod == 1) {
                    # code...
                    

                    $brotrx = brodevpick($user->id, $data->qty,$data->pickupdate,$plan->id);
                }else if ($data->shipmethod == 2) {
                    $brotrx = brodev($user->id, $data->qty,$data->alamat_id,$plan->id);

                }else{
                    $brotrx = brodevdinaran($user->id, $data->qty,$plan->id);

                }
            }
                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $data->qty,
                    'trx_type' => '-',
                    'details' => 'Purchased ' . $plan->name . ' For '.$data->qty.' BRO',
                    'remark' => 'purchased_plan',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);
    
                // dd($user);
            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                sendEmail2($user->id, 'plan_purchased', [
                    'plan' => $plan->name. ' For '.$data->qty.' BRO',
                    'amount' => getAmount($plan->price * $data->qty),
                    'currency' => $gnl->cur_text,
                    'trx' => $trx->trx,
                    'post_balance' => getAmount($user->balance),
                    'link' => route('user.plan.select.type',[$user->id,$brotrx]),
                ]);
            }


            if ($oldPlan == 0) {
                updatePaidCount2($user->id);
            }
            $details = $user->username . ' Subscribed to ' . $plan->name . ' plan.';

            referralCommission2($user->id, $details, $data->qty);

        }
    }
    public function roSuccess($trx)
    {
        $gnl = GeneralSetting::first();
        $data = DirectPlan::where('trx', $trx)->first();
        if ($data->status == 2) {
            
            $data->status = 1;
            $data->save();

            $plan = Plan::where('id', $data->plan_id)->first();

            $user = user::where('id', $data->user_id)->first();
            $ref_user = User::where('no_bro', $data->referral)->first();
            $up_user = User::where('no_bro', $data->upline)->first();        

            $oldPlan = $user->plan_id;

            $user->bro_qty += ($data->qty);
            $user->gacha_qty += $data->qty;
            $user->save();

            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {

                if ($data->shipmethod == 1) {

                    $brotrx = brodevpick($user->id, $data->qty,$data->pickupdate,$plan->id);
                }else if ($data->shipmethod == 2) {
                    $brotrx = brodev($user->id, $data->qty,$data->alamat_id,$plan->id);

                }else{
                    $brotrx = brodevdinaran($user->id, $data->qty,$plan->id);

                }
            }
                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $data->qty,
                    'trx_type' => '-',
                    'details' => 'Repeat Order for '.$data->qty.' BRO',
                    'remark' => 'repeat_order',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);

            $details = $user->username . ' Repeat Order Of '.$data->qty.' qty';

            referralCommission2($user->id, $details, $data->qty);

        }
    }

    // public function successPage($id){
    //     $page_title = 'Deposit success';
    //     $data = Deposit::where('id', $id)->where('user_id',auth()->user()->id)->first();
    //     if ($data) {
    //         # code...
    //         // $this->userDataUpdate2($data->trx);
    //         return view($this->activeTemplate . 'user.manual_payment.thankyou', compact( 'page_title'));
    //     }else{
    //         return abort(404);
    //     }
    // }
    public function successPage($id){
        $page_title = 'Deposit success';
        $data = DirectPlan::where('id', $id)->where('user_id',auth()->user()->id)->first();
        $data2 = Deposit::where('id', $id)->where('user_id',auth()->user()->id)->first();
        if ($data || $data2) {
            # code...
            // $this->userDataUpdate2($data->trx);
            return view($this->activeTemplate . 'user.manual_payment.thankyou', compact( 'page_title'));
        }else{
            return abort(404);
        }
    }
    public function failedPage($id){
        $page_title = 'Deposit Expired';
        $data = Deposit::where('id', $id)->where('user_id',auth()->user()->id)->first();
        if ($data) {
            # code...
            // $data->status = 3;
            // $data->admin_feedback =  "Deposit Expired";
            return view($this->activeTemplate . 'user.manual_payment.failed', compact( 'page_title'));
        }else{
            return abort(404);
        }
    }


    // public function manualDepositConfirm()
    // {
    //     $track = session()->get('Track');
    //     $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
    //     if (!$data) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     if ($data->status != 0) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     if ($data->method_code > 999) {

    //         $page_title = 'Deposit Confirm';
    //         $method = $data->gateway_currency();
    //         return view($this->activeTemplate . 'user.manual_payment.manual_confirm', compact('data', 'page_title', 'method'));
    //     }
    //     abort(404);
    // }

    public function manualDepositUpdate(Request $request)
    {
        $track = session()->get('Track');
        $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
        if (!$data) {
            return redirect()->route(gatewayRedirectUrl());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrl());
        }

        $params = json_decode($data->gateway_currency()->gateway_parameter);

        $rules = [];
        $inputField = [];
        $verifyImages = [];

        if ($params != null) {
            foreach ($params as $key => $cus) {
                $rules[$key] = [$cus->validation];
                if ($cus->type == 'file') {
                    array_push($rules[$key], 'image');
                    array_push($rules[$key], 'mimes:jpeg,jpg,png');
                    array_push($rules[$key], 'max:2048');

                    array_push($verifyImages, $key);
                }
                if ($cus->type == 'text') {
                    array_push($rules[$key], 'max:191');
                }
                if ($cus->type == 'textarea') {
                    array_push($rules[$key], 'max:300');
                }
                $inputField[] = $key;
            }
        }


        $this->validate($request, $rules);


        $directory = date("Y")."/".date("m")."/".date("d");
        $path = imagePath()['verify']['deposit']['path'].'/'.$directory;
        $collection = collect($request);
        $reqField = [];
        if ($params != null) {
            foreach ($collection as $k => $v) {
                foreach ($params as $inKey => $inVal) {
                    if ($k != $inKey) {
                        continue;
                    } else {
                        if ($inVal->type == 'file') {
                            if ($request->hasFile($inKey)) {
                                try {
                                    $reqField[$inKey] = [
                                        'field_name' => $directory.'/'.uploadImage($request[$inKey], $path),
                                        'type' => $inVal->type,
                                    ];
                                } catch (\Exception $exp) {
                                    $notify[] = ['error', 'Could not upload your ' . $inKey];
                                    return back()->withNotify($notify)->withInput();
                                }
                            }
                        } else {
                            $reqField[$inKey] = $v;
                            $reqField[$inKey] = [
                                'field_name' => $v,
                                'type' => $inVal->type,
                            ];
                        }
                    }
                }
            }
            $data->detail = $reqField;
        } else {
            $data->detail = null;
        }



        $data->status = 2; // pending
        $data->save();


        $adminNotification = new AdminNotification();
        $adminNotification->user_id = $data->user->id;
        $adminNotification->title = 'Deposit request from '.$data->user->username;
        $adminNotification->click_url = route('admin.deposit.details',$data->id);
        $adminNotification->save();

        $gnl = GeneralSetting::first();
        notify($data->user, 'DEPOSIT_REQUEST', [
            'method_name' => $data->gateway_currency()->name,
            'method_currency' => $data->method_currency,
            'method_amount' => getAmount($data->final_amo),
            'amount' => getAmount($data->amount),
            'charge' => getAmount($data->charge),
            'currency' => $gnl->cur_text,
            'rate' => getAmount($data->rate),
            'trx' => $data->trx
        ]);

        $notify[] = ['success', 'You have deposit request has been taken.'];
        return redirect()->route('user.deposit.history')->withNotify($notify);
    }

    public static function userDataUpdate3($trx)
    {
        $gnl = GeneralSetting::first();
        $data = Deposit::where('trx', $trx)->first();
        if ($data->status == 2) {
            $data->status = 1;
            $data->save();

            $user = User::find($data->user_id);
            $user->balance += $data->amount;
            $user->save();
            $gateway = $data->gateway;

            $transaction = new Transaction();
            $transaction->user_id = $data->user_id;
            $transaction->amount = $data->amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = getAmount($data->charge);
            $transaction->trx_type = '+';
            $transaction->details = 'Deposit Via ' . $data->gateway_currency()->name;
            $transaction->trx = $data->trx;
            $transaction->save();

            $adminNotification = new AdminNotification();
            $adminNotification->user_id = $user->id;
            $adminNotification->title = 'Deposit successful via '.$data->gateway_currency()->name;
            $adminNotification->click_url = route('admin.deposit.successful');
            $adminNotification->save();



        }
    }
}
