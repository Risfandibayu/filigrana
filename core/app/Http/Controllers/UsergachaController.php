<?php

namespace App\Http\Controllers;

use App\Models\usergacha;
use App\Http\Controllers\Controller;
use App\Models\gachamasterdetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsergachaController extends Controller
{
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $reque st
     * @return \Illuminate\Http\Response
     */
    public function filiGotIt(){
        $kuota = Auth::user()->gacha_qty;
        $gacha = UserGacha::join('gachamasterdetails','gachamasterdetails.id','usergachas.gachamasterdetail_id')->where('gachamasterdetails.gacha_id',1)->orderBy('usergachas.id','DESC')->get();
        $messages = $gacha->map(function($item) {
            return 'Selamat kepada ' . cekuser($item->user_id) . ' telah mendapatkan ' . cekprize($item->gachamasterdetail_id) . ' di FILI GOT IT!';
        });
        return view($this->activeTemplate .'user.gacha.index',compact('kuota','messages'));
    }
    public function filiGotItv2(){
        $kuota = Auth::user()->gacha_qty;
        $gacha = UserGacha::join('gachamasterdetails','gachamasterdetails.id','usergachas.gachamasterdetail_id')->where('gachamasterdetails.gacha_id',2)->orderBy('usergachas.id','DESC')->get();
        $messages = $gacha->map(function($item) {
            return 'Selamat kepada ' . cekuser($item->user_id) . ' telah mendapatkan ' . cekprize($item->gachamasterdetail_id) . ' di FILI GOT IT V2!';
        });
        return view($this->activeTemplate .'user.gacha.indexv2',compact('kuota','messages'));
    }

    public function store(Request $request)
{
    if (Auth::user()->gacha_qty < 1) {
        return response()->json(['error' => 'Tiket redeem anda kosong!, Silahkan lakukan repeat order untuk mendapatkan tiket redeem',
        'links'=>route('user.plan.index')    
        ], 400);
    }

    $userId = Auth::user()->id;
    $item = performGacha($userId);

    if (!$item) {
        return response()->json(['error' => 'Empty item'], 400);
    }

    return response()->json(['message' => 'Gacha successful', 'item' => $item], 200);
}
    public function storev2(Request $request)
{
    if (Auth::user()->gacha_qty < 1) {
        return response()->json(['error' => 'Tiket redeem anda kosong!, Silahkan lakukan repeat order untuk mendapatkan tiket redeem',
        'links'=>route('user.plan.index')    
        ], 400);
    }

    $userId = Auth::user()->id;
    $item = performGachav2($userId);

    if (!$item) {
        return response()->json(['error' => 'Empty item'], 400);
    }

    return response()->json(['message' => 'Gacha successful', 'item' => $item], 200);
}

public function claimv2(Request $request){
    $userId = Auth::user()->id;
    $successful = false;

    DB::transaction(function () use ($userId, $request,&$successful) {
        // Reload the selected item for accurate decrement within a transaction
        $item = Gachamasterdetail::find($request->gachaid);
        if ($item->qty > 0) {
            Usergacha::create([
                'user_id' => $userId,
                'gachamasterdetail_id' => $item->id,
            ]);
            $item->decrement('qty');
            // if ($item->id == 1) {
            //     gachabro($userId, 3);
            // }
            // if ($item->id == 2) {
            //     gachabro($userId, 1);
            // }
            // if ($item->id == 3) {
            //     gachaproduct($userId, 36);
            // }
            // if ($item->id == 4) {
            //     gachaproduct($userId, 35);
            // }
            // if ($item->id == 5) {
            //     gachaproduct($userId, 37);
            // }

            // Decrement gacha_qty for the user
            $user = User::find($userId);
            $user->decrement('gacha_qty');

            $successful = true;
        }
    });

    if (!$successful) {
        return response()->json(['error' => 'Error'], 400);
    }

    return response()->json(['message' => 'Gacha successful' , 'status'=> 200], 200);
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function show(usergacha $usergacha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function edit(usergacha $usergacha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usergacha $usergacha)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function destroy(usergacha $usergacha)
    {
        //
    }
}
