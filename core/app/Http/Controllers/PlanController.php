<?php

namespace App\Http\Controllers;

use App\Models\alamat;
use App\Models\brodev;
use App\Models\Brodevdetail;
use App\Models\BvLog;
use App\Models\dinaran_acc;
use App\Models\DirectPlan;
use App\Models\GeneralSetting;
use App\Models\Gold;
use App\Models\Plan;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserExtra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;

class PlanController extends Controller
{

    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }

    function planIndex()
    {
        $data['page_title'] = "Packages";
        if (Auth::user()->plan_id != 0) {
            $data['plans'] = Plan::where('id','=',Auth::user()->plan_id)->get();
        }else{
            $data['plans'] = Plan::whereStatus(1)->where('is_delete','!=',1)->get();
        }
        $data['alamat'] = alamat::where('user_id','=',Auth::user()->id)->get();
        $data['dinaran'] = dinaran_acc::where('user_id','=',Auth::user()->id)->count();
        $data['payment'] = DirectPlan::where('user_id','=',Auth::user()->id)->where('status',2)->get();
        
        return view($this->activeTemplate . '.user.plan', $data);

    }

    function planStoreXendit(Request $request){
        $this->validate($request, [
            'plan_id' => 'required|integer',
            'referral' => 'required',
            'position' => 'required',
            'qty' => 'required',
            'position' => 'required',
            'shipmethod' => 'required',
            'payment' => 'required',
        ]);

        $plan = Plan::where('id', $request->plan_id)->firstOrFail();

        
        if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
            
            if (!$request->shipmethod) {
                # code...
                $notify[] = ['error', 'Please select shipping method first'];
                return back()->withNotify($notify);
            }

            if ($request->shipmethod == 1) {
                # code...
                if (!$request->pickdate) {
                    # code...
                    $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                    return back()->withNotify($notify);
                }
                if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                    # code...
                    $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                    return back()->withNotify($notify);
                }
            }else if ($request->shipmethod == 2) {
                if (!$request->alamat) {
                    # code...
                    $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
                
                $alamat = alamat::where('id', $request->alamat)->first();
                if (!$alamat->kode_pos) {
                    # code...
                    $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                    return redirect()->back()->withNotify($notify);
                }
            }else{
                $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                if (!$dinaran) {
                    # code...
                    $notify[] = ['error', 'Please input dinaran account first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
            }
        }

        $user = User::find(Auth::id());
        $ref_user = User::where('no_bro', $request->referral)->first();
        $up_user = User::where('no_bro', $request->upline)->first();
        if (!$up_user && $request->upline) {
            $notify[] = ['error', 'Invalid Direct BRO Number.'];
            return back()->withNotify($notify);
        }

        if ($up_user) {
            # code...
            $cek_pos = User::where('pos_id', $up_user->id)->where('position',$request->position)->first();
    
            if(!treeFilter($up_user->id,$ref_user->id)){
                $notify[] = ['error', 'Refferal and Direct BRO number not in the same tree.'];
                return back()->withNotify($notify);
            }
            
            if ($cek_pos) {
                $notify[] = ['error', 'Node you input is already filled.'];
                return back()->withNotify($notify);
            }
        }

        // if ($up_user == null) {
        //     $notify[] = ['error', 'Invalid Upline BRO Number.'];
        //     return back()->withNotify($notify);
        // }

        if ($ref_user == null) {
            $notify[] = ['error', 'Invalid Refferal BRO Number.'];
            return back()->withNotify($notify);
        }
        

        if($ref_user->no_bro == $user->no_bro){
            $notify[] = ['error', 'Invalid Input BRO Number. You can`t input your own BRO number.'];
            return back()->withNotify($notify);
        }


        // if ($user->balance < ($plan->price * $request->qty)) {
        //     $notify[] = ['error', 'Insufficient Balance'];
        //     return back()->withNotify($notify);
        // }

        if ($request->payment == 'balance') {

            if ($user->balance < ($plan->price * $request->qty)) {
                $notify[] = ['error', 'Insufficient Balance'];
                return back()->withNotify($notify);
            }
            
            $bplan = $this->planStoreBalance($request);

            if ($bplan == 'Success') {
                # code...
                $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
                // return redirect()->route('user.home')->withNotify($notify);
            return redirect()->route('user.report.BroDeliveryLog')->withNotify($notify);

            }else{
                $notify[] = ['error', $bplan];
                return back()->withNotify($notify);
            }
        }

        $dplannotdone = DirectPlan::where('user_id', $user->id)->where('status',2)->first();
        if ($dplannotdone) {
            $notify[] = ['error', 'You still have transactions that have not been completed'];
            return back()->withNotify($notify);
        }

        // dd($request->all());
        $dplan = new DirectPlan();
        $dplan->trx = getTrx();
        $dplan->user_id = Auth::user()->id;
        $dplan->plan_id = $request->plan_id;
        $dplan->qty = $request->qty;
        $dplan->total = $plan->price * $request->qty;
        $dplan->referral = $request->referral;
        $dplan->upline = $request->upline;
        $dplan->position = $request->position;
        $dplan->shipmethod = $request->shipmethod;
        $dplan->pickupdate = date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"));
        $dplan->alamat_id = $request->alamat;
        $dplan->type = 'plan';
        $dplan->save();

        
        Xendit::setApiKey(config('xendit.key_auth'));

        $params = ['external_id' => (string) "plan-".$dplan->trx,
        // 'payer_email' => $user->email,
        'description' => $plan->name,
        'amount' => intval($dplan->total),
        'invoice_duration' => 604800,
        'customer' => [
            'given_names' => $user->firstname,
            'surname' => $user->lastname,
            'email' => $user->email,
            'mobile_number' => $user->mobile,
        ],
        'success_redirect_url' => env('APP_URL').'/success/'.$dplan->id,
        'failure_redirect_url' => env('APP_URL').'/failed/'.$dplan->id,
        ];        

        $createInvoice = \Xendit\Invoice::create($params);
        // dd($createInvoice);


        $dplan->status = 2;
        $dplan->url = $createInvoice['invoice_url'];
        $dplan->expired = $createInvoice['expiry_date'];
        $dplan->save(); 
        return redirect()->to($createInvoice['invoice_url']);

        
    }
    function planStore(Request $request){
        $this->validate($request, [
            'plan_id' => 'required|integer',
            'referral' => 'required',
            'position' => 'required',
            'qty' => 'required',
            'position' => 'required',
            'shipmethod' => 'required',
            'payment' => 'required',
        ]);

        $plan = Plan::where('id', $request->plan_id)->firstOrFail();

        
        if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
            
            if (!$request->shipmethod) {
                # code...
                $notify[] = ['error', 'Please select shipping method first'];
                return back()->withNotify($notify);
            }

            if ($request->shipmethod == 1) {
                # code...
                if (!$request->pickdate) {
                    # code...
                    $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                    return back()->withNotify($notify);
                }
                if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                    # code...
                    $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                    return back()->withNotify($notify);
                }
            }else if ($request->shipmethod == 2) {
                if (!$request->alamat) {
                    # code...
                    $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
                
                $alamat = alamat::where('id', $request->alamat)->first();
                if (!$alamat->kode_pos) {
                    # code...
                    $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                    return redirect()->back()->withNotify($notify);
                }
            }else{
                $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                if (!$dinaran) {
                    # code...
                    $notify[] = ['error', 'Please input dinaran account first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
            }
        }

        $user = User::find(Auth::id());
        $ref_user = User::where('no_bro', $request->referral)->first();
        $up_user = User::where('no_bro', $request->upline)->first();
        if (!$up_user && $request->upline) {
            $notify[] = ['error', 'Invalid Direct BRO Number.'];
            return back()->withNotify($notify);
        }

        if ($up_user) {
            # code...
            $cek_pos = User::where('pos_id', $up_user->id)->where('position',$request->position)->first();
    
            if(!treeFilter($up_user->id,$ref_user->id)){
                $notify[] = ['error', 'Refferal and Direct BRO number not in the same tree.'];
                return back()->withNotify($notify);
            }
            
            if ($cek_pos) {
                $notify[] = ['error', 'Node you input is already filled.'];
                return back()->withNotify($notify);
            }
        }

        // if ($up_user == null) {
        //     $notify[] = ['error', 'Invalid Upline BRO Number.'];
        //     return back()->withNotify($notify);
        // }

        if ($ref_user == null) {
            $notify[] = ['error', 'Invalid Refferal BRO Number.'];
            return back()->withNotify($notify);
        }
        

        if($ref_user->no_bro == $user->no_bro){
            $notify[] = ['error', 'Invalid Input BRO Number. You can`t input your own BRO number.'];
            return back()->withNotify($notify);
        }


        // if ($user->balance < ($plan->price * $request->qty)) {
        //     $notify[] = ['error', 'Insufficient Balance'];
        //     return back()->withNotify($notify);
        // }

        if ($request->payment == 'balance') {

            if ($user->balance < ($plan->price * $request->qty)) {
                $notify[] = ['error', 'Insufficient Balance'];
                return back()->withNotify($notify);
            }
            
            $bplan = $this->planStoreBalance($request);

            if ($bplan == 'Success') {
                # code...
                $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
                // return redirect()->route('user.home')->withNotify($notify);
            return redirect()->route('user.report.BroDeliveryLog')->withNotify($notify);

            }else{
                $notify[] = ['error', $bplan];
                return back()->withNotify($notify);
            }
        }

        $dplannotdone = DirectPlan::where('user_id', $user->id)->where('status',2)->first();
        if ($dplannotdone) {
            $notify[] = ['error', 'You still have transactions that have not been completed'];
            return back()->withNotify($notify);
        }

        // dd($request->all());
        $dplan = new DirectPlan();
        $dplan->trx = getTrx();
        $dplan->user_id = Auth::user()->id;
        $dplan->plan_id = $request->plan_id;
        $dplan->qty = $request->qty;
        $dplan->total = $plan->price * $request->qty;
        $dplan->referral = $request->referral;
        $dplan->upline = $request->upline;
        $dplan->position = $request->position;
        $dplan->shipmethod = $request->shipmethod;
        $dplan->pickupdate = date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"));
        $dplan->alamat_id = $request->alamat;
        $dplan->type = 'plan';
        $dplan->save();

        
        date_default_timezone_set('Asia/Jakarta'); // Set zona waktu sesuai dengan kebutuhan Anda

        list($microseconds, $seconds) = explode(' ', microtime());
        $milliseconds = sprintf('%03d', round($microseconds * 1000));
        $formatted_date = date('Y-m-d\TH:i:s.', $seconds) . $milliseconds . date('P', $seconds);

        $track = session()->get('Track');
        $data = $dplan;
        if (!$data) {
            return redirect()->route(gatewayRedirectUrlPlan());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrlPlan());
        }

        // Contoh data
        $httpMethod = "POST";
        $endpointUrl = "/payment/v2/h5/createLink";
        $timestamp = $formatted_date; // Misalnya timestamp dari header
        $mid = env('MID');
        $trxid = $data->id;
        $body = array(
            "merchantId" => $mid,
            "merchantTradeNo" => 'plan-'.$data->trx,
            "requestId" => $trxid,
            "amount" => intval($data->total),
            "productName" => "Deposit",
            "payer" => $user->username,
            "phoneNumber" => $user->mobile,
            "notifyUrl" => route('deposit.manual.callback2'),
            "redirectUrl" => route(gatewayRedirectUrlPlan()),
        ); // Contoh body JSON
        $privateKeyPath = base_path('private-key.pem');
        $privateKey = file_get_contents($privateKeyPath);

        // Langkah-langkah:
        // 1. Minify body JSON
        $minifiedBody = minifyJsonBody(json_encode($body));

        // 2. Membuat stringContent
        $stringContent = createStringContent($httpMethod, $endpointUrl, $minifiedBody, $timestamp);

        // 3. Membuat signature
        $signature = createSignature($stringContent, $privateKey);

        // Hasil signature



        $data_string = json_encode($body);

        // URL tujuan permintaan
        // $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl;
        if (env('APP_ENV') != 'production') {
            $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl; // for development mode
        }else{
            $url = 'https://pay.paylabs.co.id'.$endpointUrl; // for production mode
        }

        // Konfigurasi cURL
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=utf-8',
            'X-TIMESTAMP: '.$timestamp,
            'X-SIGNATURE: '.$signature,
            'X-PARTNER-ID: '.$mid,
            'X-REQUEST-ID: '.$trxid
            )
        );

        // Eksekusi permintaan dan ambil respons
        $result = curl_exec($ch);

        // Cek jika terjadi kesalahan
        if(curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        // Tutup koneksi cURL
        curl_close($ch);

        $response = json_decode($result,true);


        // echo "timestamp: ". $formatted_date."<br><br>";
        // echo "url : ".$url."<br><br>";
        // echo "body : ".$minifiedBody."<br><br>";
        // echo "stringContent : ".$stringContent."<br><br>";
        // echo 'X-TIMESTAMP : '.$timestamp."<br>";
        // echo "X-SIGNATURE : $signature"."<br>";
        // echo 'X-PARTNER-ID : '.$mid."<br>";
        // echo 'X-REQUEST-ID : '.$trxid."<br><br>";
        // echo "response : ".$result;
        // dd(json_decode($result,true));
        if ($response['errCode'] == "0") {
            # code...
            // $time = $response['expiredTime'];

            // // Memecah data waktu menjadi bagian-bagian yang relevan
            // $year = substr($time, 0, 4);
            // $month = substr($time, 4, 2);
            // $day = substr($time, 6, 2);
            // $hour = substr($time, 8, 2);
            // $minute = substr($time, 10, 2);
            // $second = substr($time, 12, 2);

            // // Format data waktu ke dalam format yang sesuai dengan tipe kolom datetime (YYYY-MM-DD HH:MM:SS)
            // $expireTime = "$year-$month-$day $hour:$minute:$second";

            $data->status = 2;
            // $data->paylabs_exptime = $expireTime;
            // $data->paylabs_platformtradeno = $response['platformTradeNo'];
            // $data->paylabs_productname = $response['productName'];
            // $data->paylabs_va = $response['productName'];
            $data->paylabs_url = $response['url'];
            $data->save(); 
            return redirect()->to($data->paylabs_url);
            // dd($response);
            // return redirect()->route('user.deposit.payment', $data->trx);

        }else{
            $notify[] = ['error', 'Error '.$response['errCode']];
            return redirect()->back()->withNotify($notify);
        }

        
    }

    function cancelPlan($id){
        $dplan = DirectPlan::where('id',$id)->first();
        $dplan->status = 3;
        $dplan->admin_feedback = 'Cancelled by User';
        $dplan->save();

        $notify[] = ['success', 'Payment Canceled Successfully'];
        return back()->withNotify($notify);
    }

    function planStore2(Request $request)
    {
        // dd($request->all());
        // dd(date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")));
        // dd(now() < date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")) ? 'ok' : 'no');
        $plan = Plan::where('id', $request->plan_id)->firstOrFail();

        
        if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
            
            if (!$request->shipmethod) {
                # code...
                $notify[] = ['error', 'Please select shipping method first'];
                return back()->withNotify($notify);
            }

            if ($request->shipmethod == 1) {
                # code...
                if (!$request->pickdate) {
                    # code...
                    $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                    return back()->withNotify($notify);
                }
                if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                    # code...
                    $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                    return back()->withNotify($notify);
                }
            }else if ($request->shipmethod == 2) {
                if (!$request->alamat) {
                    # code...
                    $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
                
                $alamat = alamat::where('id', $request->alamat)->first();
                if (!$alamat->kode_pos) {
                    # code...
                    $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                    return redirect()->back()->withNotify($notify);
                }
            }else{
                $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                if (!$dinaran) {
                    # code...
                    $notify[] = ['error', 'Please input dinaran account first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }
            }
        }

        $this->validate($request, [
            'plan_id' => 'required|integer',
            'referral' => 'required',
            'position' => 'required',
            // 'alamat' => 'required',
        ]);
        // dd($request->all());
        
        $gnl = GeneralSetting::first();
        // dd(date('Y-m-d,H:i:s'));

        $brolimit = user::where('plan_id','!=',0)->count();
        // dd($brolimit);
        if (date('Y-m-d,H:i:s') > '2022-09-02,23:59:59') {
            # code...
            // dd('s');
            $g1 = 2;
            $g2 = 2;
            $g3 = 2;
            $g4 = 2;
            $g5 = 2;
            $g6 = 2;
            $tot = 12;
        }else{
            // dd('w');
            $g1 = 2;
            $g2 = 2;
            $g3 = 2;
            $g4 = 2;
            $g5 = 2;
            $g6 = 2;
            $tot = 12;
        }


        $user = User::find(Auth::id());
        $ref_user = User::where('no_bro', $request->referral)->first();
        $up_user = User::where('no_bro', $request->upline)->first();
        if (!$up_user && $request->upline) {
            $notify[] = ['error', 'Invalid Direct BRO Number.'];
            return back()->withNotify($notify);
        }

        if ($up_user) {
            # code...
            $cek_pos = User::where('pos_id', $up_user->id)->where('position',$request->position)->first();
    
            if(!treeFilter($up_user->id,$ref_user->id)){
                $notify[] = ['error', 'Refferal and Direct BRO number not in the same tree.'];
                return back()->withNotify($notify);
            }
            
            if ($cek_pos) {
                $notify[] = ['error', 'Node you input is already filled.'];
                return back()->withNotify($notify);
            }
        }

        // if ($up_user == null) {
        //     $notify[] = ['error', 'Invalid Upline BRO Number.'];
        //     return back()->withNotify($notify);
        // }

        if ($ref_user == null) {
            $notify[] = ['error', 'Invalid Refferal BRO Number.'];
            return back()->withNotify($notify);
        }
        

        if($ref_user->no_bro == $user->no_bro){
            $notify[] = ['error', 'Invalid Input BRO Number. You can`t input your own BRO number.'];
            return back()->withNotify($notify);
        }


        if ($user->balance < ($plan->price * $request->qty)) {
            $notify[] = ['error', 'Insufficient Balance'];
            return back()->withNotify($notify);
        }

        // dd($request->all());

        DB::beginTransaction();

        try {
            $oldPlan = $user->plan_id;
            
            $pos = getPosition($ref_user->id, $request->position);
            $user->no_bro       = generateUniqueNoBro();
            $user->ref_id= $ref_user->id;
            // $user->pos_id= $pos['pos_id'];
            // $user->pos_id= $up_user->id;
            $user->pos_id= isset($up_user->id) ? $up_user->id : $pos['pos_id'];
            $user->position= $request->position;
            $user->plan_id = $plan->id;
            $user->balance -= ($plan->price * $request->qty);
            $user->total_invest += ($plan->price * $request->qty);
            $user->bro_qty += ($request->qty - 1);
            $user->is_matriks = $request->qty>14 ? 1 : 0;
            $user->gacha_qty += $request->qty;
            $user->save();

            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                # code...

            // $gold = Gold::where('user_id',Auth::user()->id)->first();
            // $gold1 = Gold::where('user_id',Auth::user()->id)->where('prod_id',1)->first();
            // $gold2 = Gold::where('user_id',Auth::user()->id)->where('prod_id',2)->first();
            // $gold3 = Gold::where('user_id',Auth::user()->id)->where('prod_id',3)->first();
            // $gold4 = Gold::where('user_id',Auth::user()->id)->where('prod_id',4)->first();
            // $gold5 = Gold::where('user_id',Auth::user()->id)->where('prod_id',5)->first();
            // $gold6 = Gold::where('user_id',Auth::user()->id)->where('prod_id',6)->first();

            // if($gold){
            //     if($gold1){
            //         $gold1->qty += $g1 * $request->qty;
            //         $gold1->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 1;
            //         $newg->qty = $g1 * $request->qty;
            //         $newg->save();
            //     }

            //     if($gold2){
            //         $gold2->qty += $g2 * $request->qty;
            //         $gold2->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 2;
            //         $newg->qty = $g2 * $request->qty;
            //         $newg->save();
            //     }

            //     if($gold3){
            //         $gold3->qty += $g3 * $request->qty;
            //         $gold3->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 3;
            //         $newg->qty = $g3 * $request->qty;
            //         $newg->save();
            //     }

            //     if($gold4){
            //         $gold4->qty += $g4 * $request->qty;
            //         $gold4->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 4;
            //         $newg->qty = $g4 * $request->qty;
            //         $newg->save();
            //     }

            //     if($gold5){
            //         $gold5->qty += $g5 * $request->qty;
            //         $gold5->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 5;
            //         $newg->qty = $g5 * $request->qty;
            //         $newg->save();
            //     }

            //     if($gold6){
            //         $gold6->qty += $g6 * $request->qty;
            //         $gold6->save();
            //     }else{
            //         $newg = new Gold();
            //         $newg->user_id = Auth::user()->id;
            //         $newg->prod_id = 6;
            //         $newg->qty = $g6 * $request->qty;
            //         $newg->save();
            //     }


            // }else{
            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 1;
            //     $newg->qty = $g1 * $request->qty;
            //     $newg->save();

            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 2;
            //     $newg->qty = $g2 * $request->qty;
            //     $newg->save();

            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 3;
            //     $newg->qty = $g3 * $request->qty;
            //     $newg->save();

            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 4;
            //     $newg->qty = $g4 * $request->qty;
            //     $newg->save();

            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 5;
            //     $newg->qty = $g5 * $request->qty;
            //     $newg->save();

            //     $newg = new Gold();
            //     $newg->user_id = Auth::user()->id;
            //     $newg->prod_id = 6;
            //     $newg->qty = $g6 * $request->qty;
            //     $newg->save();
            // }

                if ($request->shipmethod == 1) {
                    # code...
                    

                    $brotrx = brodevpick(Auth::user()->id, $request->qty,date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")),$plan->id);
                }else if ($request->shipmethod == 2) {
                    $brotrx = brodev(Auth::user()->id, $request->qty,$request->alamat,$plan->id);

                }else{
                    $brotrx = brodevdinaran(Auth::user()->id, $request->qty,$plan->id);

                }
            }

            // if ($plan->id == 1) {
            //     $trx = $user->transactions()->create([
            //         'amount' => $plan->price * $request->qty,
            //         'trx_type' => '-',
            //         'details' => 'Purchased ' . $plan->name . ' For '.$request->qty.' BRO and Get '.$request->qty * $tot .' Pieces Jewelry',
            //         'remark' => 'purchased_plan',
            //         'trx' => getTrx(),
            //         'post_balance' => getAmount($user->balance),
            //     ]);

            //     // dd($user);

            //     sendEmail2($user->id, 'plan_purchased', [
            //         'plan' => $plan->name. ' For '.$request->qty.' BRO and Get '.$request->qty * $tot .' Pieces Jewelry',
            //         'amount' => getAmount($plan->price * $request->qty),
            //         'currency' => $gnl->cur_text,http://localhost/filigrana/user/profile-setting
            //         'trx' => $trx->trx,
            //         'post_balance' => getAmount($user->balance),
            //         'link' => route('user.plan.select.type',[$user->id,$brotrx]),
            //     ]);
            // }else{
                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $request->qty,
                    'trx_type' => '-',
                    'details' => 'Purchased ' . $plan->name . ' For '.$request->qty.' BRO',
                    'remark' => 'purchased_plan',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);
    
                // dd($user);
            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                sendEmail2($user->id, 'plan_purchased', [
                    'plan' => $plan->name. ' For '.$request->qty.' BRO',
                    'amount' => getAmount($plan->price * $request->qty),
                    'currency' => $gnl->cur_text,
                    'trx' => $trx->trx,
                    'post_balance' => getAmount($user->balance),
                    'link' => route('user.plan.select.type',[$user->id,$brotrx]),
                ]);
            }
            // }


            if ($oldPlan == 0) {
                updatePaidCount2($user->id);
            }
            $details = Auth::user()->username . ' Subscribed to ' . $plan->name . ' plan.';

            // updateBV($user->id, $plan->bv, $details);

            // if ($plan->tree_com > 0) {
            //     treeComission($user->id, $plan->tree_com, $details);
            // }

            $post = [
                'email' => $user->email,
                'bro' => $user->no_bro,
            ];
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => env('MG_API'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            referralCommission2($user->id, $details, $request->qty);
            DB::commit();

            $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
            return redirect()->route('user.home')->withNotify($notify);

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                $notify[] = ['error', 'Purchase failed, please try again.'.$e];
                return redirect()->back()->withNotify($notify);
            }

    }


    function planStoreBalance(Request $request)
    {
        // dd($request->all());
        // dd(date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")));
        // dd(now() < date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")) ? 'ok' : 'no');
        DB::beginTransaction();

        try {
        $plan = Plan::where('id', $request->plan_id)->first();

        
        $gnl = GeneralSetting::first();
        // dd(date('Y-m-d,H:i:s'));

        $brolimit = user::where('plan_id','!=',0)->count();
        // dd($brolimit);
        if (date('Y-m-d,H:i:s') > '2022-09-02,23:59:59') {
            # code...
            // dd('s');
            $g1 = 2;
            $g2 = 2;
            $g3 = 2;
            $g4 = 2;
            $g5 = 2;
            $g6 = 2;
            $tot = 12;
        }else{
            // dd('w');
            $g1 = 2;
            $g2 = 2;
            $g3 = 2;
            $g4 = 2;
            $g5 = 2;
            $g6 = 2;
            $tot = 12;
        }


        $user = User::find(Auth::id());
        $ref_user = User::where('no_bro', $request->referral)->first();
        $up_user = User::where('no_bro', $request->upline)->first();
        

        // dd($request->all());

        
            $oldPlan = $user->plan_id;
            
            $pos = getPosition($ref_user->id, $request->position);
            $user->no_bro       = generateUniqueNoBro();
            $user->ref_id= $ref_user->id;
            $user->pos_id= isset($up_user->id) ? $up_user->id : $pos['pos_id'];
            $user->position= $request->position;
            $user->plan_id = $plan->id;
            $user->balance -= ($plan->price * $request->qty);
            $user->total_invest += ($plan->price * $request->qty);
            $user->bro_qty += ($request->qty - 1);
            $user->is_matriks = $request->qty>14 ? 1 : 0;
            $user->gacha_qty += $request->qty;
            $user->save();

            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                # code...

                if ($request->shipmethod == 1) {
                    # code...
                    

                    $brotrx = brodevpick(Auth::user()->id, $request->qty,date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime")),$plan->id);
                }else if ($request->shipmethod == 2) {
                    $brotrx = brodev(Auth::user()->id, $request->qty,$request->alamat,$plan->id);

                }else{
                    $brotrx = brodevdinaran(Auth::user()->id, $request->qty,$plan->id);

                }
            }

                $trx = $user->transactions()->create([
                    'amount' => $plan->price * $request->qty,
                    'trx_type' => '-',
                    'details' => 'Purchased ' . $plan->name . ' For '.$request->qty.' BRO',
                    'remark' => 'purchased_plan',
                    'trx' => getTrx(),
                    'post_balance' => getAmount($user->balance),
                ]);

            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                sendEmail2($user->id, 'plan_purchased', [
                    'plan' => $plan->name. ' For '.$request->qty.' BRO',
                    'amount' => getAmount($plan->price * $request->qty),
                    'currency' => $gnl->cur_text,
                    'trx' => $trx->trx,
                    'post_balance' => getAmount($user->balance),
                    'link' => route('user.plan.select.type',[$user->id,$brotrx]),
                ]);
            }
            // }


            if ($oldPlan == 0) {
                updatePaidCount2($user->id);
            }
            $details = Auth::user()->username . ' Subscribed to ' . $plan->name . ' plan.';

            referralCommission2($user->id, $details, $request->qty);
            DB::commit();

                return 'Success';

            } catch (\Exception $e) {
                DB::rollback();

                return $e.'error!';
            }

    }


    public function binaryCom()
    {
        $data['page_title'] = "Binary Commission";
        $data['logs'] = Transaction::where('user_id', auth()->id())->where('remark', 'binary_commission')->orderBy('id', 'DESC')->paginate(config('constants.table.default'));
        $data['empty_message'] = 'No data found';
        return view($this->activeTemplate . '.user.transactions', $data);
    }

    public function binarySummery()
    {
        $data['page_title'] = "Binary Summery";
        $data['logs'] = UserExtra::where('user_id', auth()->id())->firstOrFail();
        return view($this->activeTemplate . '.user.binarySummery', $data);
    }

    public function bvlog(Request $request)
    {

        if ($request->type) {
            if ($request->type == 'leftBV') {
                $data['page_title'] = "Left BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('position', 1)->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'rightBV') {
                $data['page_title'] = "Right BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('position', 2)->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } elseif ($request->type == 'cutBV') {
                $data['page_title'] = "Cut BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('trx_type', '-')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            } else {
                $data['page_title'] = "All Paid BV";
                $data['logs'] = BvLog::where('user_id', auth()->id())->where('trx_type', '+')->orderBy('id', 'desc')->paginate(config('constants.table.default'));
            }
        } else {
            $data['page_title'] = "BV LOG";
            $data['logs'] = BvLog::where('user_id', auth()->id())->orderBy('id', 'desc')->paginate(config('constants.table.default'));
        }

        $data['empty_message'] = 'No data found';
        return view($this->activeTemplate . '.user.bvLog', $data);
    }

    public function myRefLog()
    {
        $data['page_title'] = "My Referral";
        $data['empty_message'] = 'No data found';
        $data['logs'] = User::where('ref_id', auth()->id())
        ->select('users.*')
        ->orderBy('users.created_at','ASC')->paginate(config('constants.table.default'));
        return view($this->activeTemplate . '.user.myRef', $data);
    }

    public function myTree()
    {
        $data['tree'] = showTreePage(Auth::id());
        $data['page_title'] = "My Group Sales";
        return view($this->activeTemplate . 'user.myTree', $data);
    }


    public function otherTree(Request $request, $username = null)
    {
        if ($request->username) {
            $user = User::where('username', $request->username)->first();
        } else {
            $user = User::where('username', $username)->first();
        }
        if ($user && treeAuth($user->id, auth()->id())) {
            $data['tree'] = showTreePage($user->id);
            $data['page_title'] = "Tree of " . $user->fullname;
            return view($this->activeTemplate . 'user.myTree', $data);
        }

        $notify[] = ['error', 'Tree Not Found or You do not have Permission to view that!!'];
        return redirect()->route('user.my.tree')->withNotify($notify);

    }

    public function buyBROStore(Request $request){
        // dd($request->all());
        $this->validate($request, [
            'qtyy' => 'required|integer|min:1',
        ]);
        $user = User::find(Auth::id());
        $plan = Plan::where('id', $request->plan_id)->firstOrFail();
        $gnl = GeneralSetting::first();
        if ($user->balance < ($plan->price * $request->qtyy)) {
            $notify[] = ['error', 'Insufficient Balance'];
            return back()->withNotify($notify);
        }
        $user->balance -= ($plan->price * $request->qtyy);
        $user->total_invest += ($plan->price * $request->qtyy);
        $user->bro_qty += $request->qtyy;
        $user->gacha_qty += $request->qty;
        $user->save();

        $trx = $user->transactions()->create([
            'amount' => $plan->price * $request->qtyy,
            'trx_type' => '-',
            'details' => 'Repeat Order quantity for '.$request->qtyy.' BRO',
            'remark' => 'purchased_plan',
            'trx' => getTrx(),
            'post_balance' => getAmount($user->balance),
        ]);

        $notify[] = ['success', 'Repeat Order quantity for '.$request->qtyy.' BRO Successfully'];
        return redirect()->route('user.home')->withNotify($notify);

    }

    public function refJoin(Request $request){

        if (!Auth::check()) {
            // Jika pengguna belum login, arahkan mereka ke halaman pendaftaran
            return redirect()->route('user.register', ['ref' => $request->ref,'position' => $request->position]);
        }
        $u = User::where('no_bro',$request->ref)->first();
        if(!$u){
            $notify[] = ['error', 'BRO Refferal number not found!'];
            return redirect()->back()->withNotify($notify);
        }
        if(Auth::user()->plan_id != 0){
            $notify[] = ['error', 'You already purchase plan!'];
            return redirect()->back()->withNotify($notify);
        }
        if(Auth::user()->no_bro == $request->ref){
            $notify[] = ['error', 'You can`t use your own BRO number'];
            return redirect()->back()->withNotify($notify);
        }
        // dd($request->all());
        // $data['tree'] = showTreePage(Auth::id());
        $data['plan'] = Plan::where('status','!=',0)->get();
        $data['ref'] = $request->ref;
        $data['position'] = $request->position;
        $data['alamat'] = alamat::where('user_id','=',Auth::user()->id)->get();
        $data['page_title'] = "Purchase Plan By Refferal";
        return view($this->activeTemplate . '.user.joinref', $data);
    }

    public function selectType($user,$trx){
        $users = user::where('id',$user)->first();
        if (!$users) {
            $notify[] = ['error', 'Invalid link'];
            return redirect()->back()->withNotify($notify);
        }

        if ($user != auth()->user()->id) {
            $notify[] = ['error', 'Invalid link'];
            return redirect()->back()->withNotify($notify);
        }

        $data['fren'] = brodev::where('trx',$trx)->where('user_id',auth()->user()->id)->first();
        $data['alamat'] = alamat::where('user_id','=',Auth::user()->id)->get();
        $data['dinaran'] = dinaran_acc::where('user_id','=',Auth::user()->id)->count();

        if (!$data['fren']) {
            $notify[] = ['error', 'Invalid link'];
            return redirect()->back()->withNotify($notify);
        }

        if ($data['fren']->product != 0) {
            # code...
            $notify[] = ['error', 'Product has been selected'];
            return redirect()->back()->withNotify($notify);
        }

        $data['page_title'] = "Select Product To Be Deliver";
        return view($this->activeTemplate . '.user.prod_select',$data);
        
    }

    public function selectTypeSubmit(Request $request){
        // dd($request->all());
        $brodev = brodev::where('trx',$request->trx)->where('user_id',auth()->user()->id)->first();
        if (!$brodev) {
            $notify[] = ['error', 'BRO Delivery Not Found'];
            return redirect()->back()->withNotify($notify);
        }

        if(count($request->product) > $brodev->bro_qty ){
            $notify[] = ['error', 'Error'];
            return redirect()->back()->withNotify($notify);
        }
        if (isset($request->shipmethod)) {
                # code...

            if ($request->shipmethod == 1) {
                # code...
                if (!$request->pickdate) {
                    # code...
                    $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                    return back()->withNotify($notify);
                }
                if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                    # code...
                    $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                    return back()->withNotify($notify);
                }

                $brodev->pickupdate = date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"));
                $brodev->status = 2;
                $brodev->ship_method = 1;
                $brodev->plan_id = auth()->user()->plan_id;
                $brodev->save();
            }else if ($request->shipmethod == 2) {

                if (!$request->alamat) {
                    # code...
                    $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                    // return back()->withNotify($notify);
                    return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }

                $alm = alamat::where('id','=',$request->alamat)->first();

                $brodev->alamat = $alm->alamat;
                $brodev->nama_penerima = $alm->nama_penerima;
                $brodev->no_telp_penerima = $alm->no_telp;
                $brodev->kode_pos = $alm->kode_pos;
                $brodev->status = 2;
                $brodev->ship_method = 2;
                $brodev->plan_id = auth()->user()->plan_id;
                $brodev->save();

            }else{
                $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                if (!$dinaran) {
                        $notify[] = ['error', 'Please input dinaran account first'];
                        // return back()->withNotify($notify);
                        return redirect()->route('user.profile-setting', ['#alamat'])->withNotify($notify);
                }

                $brodev->status = 2;
                $brodev->ship_method = 3;
                $brodev->plan_id = auth()->user()->plan_id;
                $brodev->save();

            }
        }


        foreach ($request->product as $key => $value) {
            # code...
            $bdetail = new Brodevdetail();
            $bdetail->brodev_id = $brodev->id;
            $bdetail->product = $value;
            $bdetail->qty = 1;
            $bdetail->save();

            
        }

        $brodev->product = 1;
        $brodev->save();

        $notify[] = ['success', 'The product has been successfully selected'];
        return redirect()->route('user.report.BroDeliveryLog')->withNotify($notify);
    }

    public function mailpp(Request $request){
        $validator = validator($request->all(), [
            'user_id' => 'required',
            'plan_id' => 'required',
            'trx_id' => 'required',
            'qty' => 'required',
            'brodev_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=> 'fail','errors' => $validator->errors()], 422);
        }

        $plan = Plan::where('id',$request->plan_id)->first();
        if (!$plan) {
            return response()->json(['status'=> 'fail','errors' => 'The plan id not found'], 422);
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['status'=> 'fail','errors' => 'The user id not found'], 422);
        }

        $brodev = brodev::where('id',$request->brodev_id)->first();
        if (!$brodev) {
            return response()->json(['status'=> 'fail','errors' => 'The brodev_id id not found'], 422);
        }

        $trx = Transaction::where('id',$request->trx_id)->first();
        if (!$trx) {
            return response()->json(['status'=> 'fail','errors' => 'The trx_id id not found'], 422);
        }
        

        if ($plan->id == 1 || $plan->id == 5) {

            sendEmail2($user->id, 'plan_purchased', [
                'plan' => $plan->name. ' For '.$request->qty.' BRO',
                'amount' => getAmount($plan->price * $request->qty),
                'currency' => 'IDR',
                'trx' => $trx->trx,
                'post_balance' => getAmount($user->balance),
                'link' => route('user.plan.select.type',[$user->id,$brodev->trx]),
            ]);
        }

        return response()->json(['status'=>'success']);
    }

    public function repeatOrderXendit(Request $request){
        $this->validate($request, [
            'plan_id' => 'required|integer',
            'qty' => 'required',
            'shipmethod' => 'required',
            'payment' => 'required',
        ]);

        if ($request->payment == 'balance') {
            # code...
            $user = User::find(Auth::id());
            $plan = Plan::where('id', $request->plan_id)->firstOrFail();


            if ($user->balance < ($plan->price * $request->qty)) {
                $notify[] = ['error', 'Insufficient Balance'];
                return redirect()->route('user.plan.index')->withNotify($notify);
            }

            $this->repeatOrder2($request);

            $notify[] = ['success', 'Repeat Order quantity for '.$request->qty.' BRO Successfully'];
            return redirect()->route('user.plan.index')->withNotify($notify);

        }else{

            $plan = Plan::where('id', $request->plan_id)->firstOrFail();

            
            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                
                if (!$request->shipmethod) {
                    # code...
                    $notify[] = ['error', 'Please select shipping method first'];
                    return redirect()->route('user.plan.index')->withNotify($notify);
                }

                if ($request->shipmethod == 1) {
                    # code...
                    if (!$request->pickdate) {
                        # code...
                        $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                    if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                        # code...
                        $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }else if ($request->shipmethod == 2) {
                    if (!$request->alamat) {
                        # code...
                        $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                        // return redirect()->route('user.plan.index')->withNotify($notify);
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                    
                    $alamat = alamat::where('id', $request->alamat)->first();
                    if (!$alamat->kode_pos) {
                        # code...
                        $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                        return redirect()->redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }else{
                    $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                    if (!$dinaran) {
                        # code...
                        $notify[] = ['error', 'Please input dinaran account first'];
                        // return redirect()->route('user.plan.index')->withNotify($notify);
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }
            }

            $user = User::find(Auth::id());
            // if ($user->balance < ($plan->price * $request->qty)) {
            //     $notify[] = ['error', 'Insufficient Balance'];
            //     return redirect()->route('user.plan.index')->withNotify($notify);
            // }

            $dplannotdone = DirectPlan::where('user_id', $user->id)->where('status',2)->first();
            if ($dplannotdone) {
                $notify[] = ['error', 'You still have transactions that have not been completed'];
                return redirect()->route('user.plan.index')->withNotify($notify);
            }

            DB::beginTransaction();

            try {

            // dd($request->all());
            $dplan = new DirectPlan();
            $dplan->trx = getTrx();
            $dplan->user_id = Auth::user()->id;
            $dplan->plan_id = $request->plan_id;
            $dplan->qty = $request->qty;
            $dplan->total = $plan->price * $request->qty;
            $dplan->shipmethod = $request->shipmethod;
            $dplan->pickupdate = date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"));
            $dplan->alamat_id = $request->alamat;
            $dplan->type = 'ro';
            $dplan->save();

            
            Xendit::setApiKey(config('xendit.key_auth'));

            $params = ['external_id' => (string) "ro-".$dplan->trx,
            // 'payer_email' => $user->email,
            'description' => $plan->name,
            'amount' => intval($dplan->total),
            'invoice_duration' => 604800,
            'customer' => [
                'given_names' => $user->firstname,
                'surname' => $user->lastname,
                'email' => $user->email,
                'mobile_number' => $user->mobile,
            ],
            'success_redirect_url' => env('APP_URL').'/success/'.$dplan->id,
            'failure_redirect_url' => env('APP_URL').'/failed/'.$dplan->id,
            ];        

            $createInvoice = \Xendit\Invoice::create($params);
            // dd($createInvoice);


            $dplan->status = 2;
            $dplan->url = $createInvoice['invoice_url'];
            $dplan->expired = $createInvoice['expiry_date'];
            $dplan->save(); 
            DB::commit();
            return redirect()->to($createInvoice['invoice_url']);


            // $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
            // return redirect()->route('user.home')->withNotify($notify);

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                $notify[] = ['error', 'Purchase failed, please try again.'.$e];
                return redirect()->back()->withNotify($notify);
            }

        }
    }
    public function repeatOrder(Request $request){
        $this->validate($request, [
            'plan_id' => 'required|integer',
            'qty' => 'required',
            'shipmethod' => 'required',
            'payment' => 'required',
        ]);

        if ($request->payment == 'balance') {
            # code...
            $user = User::find(Auth::id());
            $plan = Plan::where('id', $request->plan_id)->firstOrFail();


            if ($user->balance < ($plan->price * $request->qty)) {
                $notify[] = ['error', 'Insufficient Balance'];
                return redirect()->route('user.plan.index')->withNotify($notify);
            }

            $this->repeatOrder2($request);

            $notify[] = ['success', 'Repeat Order quantity for '.$request->qty.' BRO Successfully'];
            return redirect()->route('user.plan.index')->withNotify($notify);

        }else{

            $plan = Plan::where('id', $request->plan_id)->firstOrFail();

            
            if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {
                
                if (!$request->shipmethod) {
                    # code...
                    $notify[] = ['error', 'Please select shipping method first'];
                    return redirect()->route('user.plan.index')->withNotify($notify);
                }

                if ($request->shipmethod == 1) {
                    # code...
                    if (!$request->pickdate) {
                        # code...
                        $notify[] = ['error', 'Please enter BRO Pack pick up date'];
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                    if (now() > date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"))) {
                        # code...
                        $notify[] = ['error', 'Date and time you enter must be later than the current one'];
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }else if ($request->shipmethod == 2) {
                    if (!$request->alamat) {
                        # code...
                        $notify[] = ['error', 'Please select the delivery address BRO pack or add the address in the profile settings menu first'];
                        // return redirect()->route('user.plan.index')->withNotify($notify);
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                    
                    $alamat = alamat::where('id', $request->alamat)->first();
                    if (!$alamat->kode_pos) {
                        # code...
                        $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
                        return redirect()->redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }else{
                    $dinaran = dinaran_acc::where('user_id','=',Auth::user()->id)->get();
                    if (!$dinaran) {
                        # code...
                        $notify[] = ['error', 'Please input dinaran account first'];
                        // return redirect()->route('user.plan.index')->withNotify($notify);
                        return redirect()->route('user.plan.index')->withNotify($notify);
                    }
                }
            }

            $user = User::find(Auth::id());
            // if ($user->balance < ($plan->price * $request->qty)) {
            //     $notify[] = ['error', 'Insufficient Balance'];
            //     return redirect()->route('user.plan.index')->withNotify($notify);
            // }

            $dplannotdone = DirectPlan::where('user_id', $user->id)->where('status',2)->first();
            if ($dplannotdone) {
                $notify[] = ['error', 'You still have transactions that have not been completed'];
                return redirect()->route('user.plan.index')->withNotify($notify);
            }

            DB::beginTransaction();

            try {

            // dd($request->all());
            $dplan = new DirectPlan();
            $dplan->trx = getTrx();
            $dplan->user_id = Auth::user()->id;
            $dplan->plan_id = $request->plan_id;
            $dplan->qty = $request->qty;
            $dplan->total = $plan->price * $request->qty;
            $dplan->shipmethod = $request->shipmethod;
            $dplan->pickupdate = date('Y-m-d H:i:s', strtotime("$request->pickdate $request->picktime"));
            $dplan->alamat_id = $request->alamat;
            $dplan->type = 'ro';
            $dplan->save();

            
            date_default_timezone_set('Asia/Jakarta'); // Set zona waktu sesuai dengan kebutuhan Anda

        list($microseconds, $seconds) = explode(' ', microtime());
        $milliseconds = sprintf('%03d', round($microseconds * 1000));
        $formatted_date = date('Y-m-d\TH:i:s.', $seconds) . $milliseconds . date('P', $seconds);

        $track = session()->get('Track');
        $data = $dplan;
        if (!$data) {
            return redirect()->route(gatewayRedirectUrlPlan());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrlPlan());
        }

        // Contoh data
        $httpMethod = "POST";
        $endpointUrl = "/payment/v2/h5/createLink";
        $timestamp = $formatted_date; // Misalnya timestamp dari header
        $mid = env('MID');
        $trxid = $data->id;
        $body = array(
            "merchantId" => $mid,
            "merchantTradeNo" => 'ro-'.$data->trx,
            "requestId" => $trxid,
            "amount" => intval($data->total),
            "productName" => "Deposit",
            "payer" => $user->username,
            "phoneNumber" => $user->mobile,
            "notifyUrl" => route('deposit.manual.callback2'),
            "redirectUrl" => route(gatewayRedirectUrlPlan()),
        ); // Contoh body JSON
        $privateKeyPath = base_path('private-key.pem');
        $privateKey = file_get_contents($privateKeyPath);

        // Langkah-langkah:
        // 1. Minify body JSON
        $minifiedBody = minifyJsonBody(json_encode($body));

        // 2. Membuat stringContent
        $stringContent = createStringContent($httpMethod, $endpointUrl, $minifiedBody, $timestamp);

        // 3. Membuat signature
        $signature = createSignature($stringContent, $privateKey);

        // Hasil signature



        $data_string = json_encode($body);

        // URL tujuan permintaan
        // $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl;
        if (env('APP_ENV') != 'production') {
            $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl; // for development mode
        }else{
            $url = 'https://pay.paylabs.co.id'.$endpointUrl; // for production mode
        }

        // Konfigurasi cURL
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=utf-8',
            'X-TIMESTAMP: '.$timestamp,
            'X-SIGNATURE: '.$signature,
            'X-PARTNER-ID: '.$mid,
            'X-REQUEST-ID: '.$trxid
            )
        );

        // Eksekusi permintaan dan ambil respons
        $result = curl_exec($ch);

        // Cek jika terjadi kesalahan
        if(curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        // Tutup koneksi cURL
        curl_close($ch);

        $response = json_decode($result,true);


        // echo "timestamp: ". $formatted_date."<br><br>";
        // echo "url : ".$url."<br><br>";
        // echo "body : ".$minifiedBody."<br><br>";
        // echo "stringContent : ".$stringContent."<br><br>";
        // echo 'X-TIMESTAMP : '.$timestamp."<br>";
        // echo "X-SIGNATURE : $signature"."<br>";
        // echo 'X-PARTNER-ID : '.$mid."<br>";
        // echo 'X-REQUEST-ID : '.$trxid."<br><br>";
        // echo "response : ".$result;
        // dd(json_decode($result,true));
        if ($response['errCode'] == "0") {
            # code...
            // $time = $response['expiredTime'];

            // // Memecah data waktu menjadi bagian-bagian yang relevan
            // $year = substr($time, 0, 4);
            // $month = substr($time, 4, 2);
            // $day = substr($time, 6, 2);
            // $hour = substr($time, 8, 2);
            // $minute = substr($time, 10, 2);
            // $second = substr($time, 12, 2);

            // // Format data waktu ke dalam format yang sesuai dengan tipe kolom datetime (YYYY-MM-DD HH:MM:SS)
            // $expireTime = "$year-$month-$day $hour:$minute:$second";

            $data->status = 2;
            // $data->paylabs_exptime = $expireTime;
            // $data->paylabs_platformtradeno = $response['platformTradeNo'];
            // $data->paylabs_productname = $response['productName'];
            // $data->paylabs_va = $response['productName'];
            $data->paylabs_url = $response['url'];
            $data->save(); 
            DB::commit();
            return redirect()->to($data->paylabs_url);

            // dd($response);
            // return redirect()->route('user.deposit.payment', $data->trx);

        }else{
            $notify[] = ['error', 'Error '.$response['errCode']];
            return redirect()->back()->withNotify($notify);
        }


            // $notify[] = ['success', 'Purchased ' . $plan->name . ' Successfully'];
            // return redirect()->route('user.home')->withNotify($notify);

            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                $notify[] = ['error', 'Purchase failed, please try again.'.$e];
                return redirect()->back()->withNotify($notify);
            }

        }
    }

    function repeatOrder2(Request $request){
        $user = User::find(Auth::id());
        $plan = Plan::where('id', $request->plan_id)->firstOrFail();
        $gnl = GeneralSetting::first();
        $user->balance -= ($plan->price * $request->qty);
        $user->total_invest += ($plan->price * $request->qty);
        $user->bro_qty += $request->qty;
        $user->gacha_qty += $request->qty;
        $user->save();

        if ($plan->id == 1 || $plan->id == 5 || $plan->id == 8 || $plan->id == 9) {

            if ($request->shipmethod == 1) {

                $brotrx = brodevpick($user->id, $request->qty,$request->pickupdate,$plan->id);
            }else if ($request->shipmethod == 2) {
                $brotrx = brodev($user->id, $request->qty,$request->alamat_id,$plan->id);

            }else{
                $brotrx = brodevdinaran($user->id, $request->qty,$plan->id);

            }
        }

        $trx = $user->transactions()->create([
            'amount' => $plan->price * $request->qty,
            'trx_type' => '-',
            'details' => 'Repeat Order for '.$request->qty.' BRO',
            'remark' => 'repeat_order',
            'trx' => getTrx(),
            'post_balance' => getAmount($user->balance),
        ]);

        $details = $user->username . ' Repeat Order Of '.$request->qty.' qty';

        referralCommission2($user->id, $details, $request->qty);

        
    }

}
