<?php

namespace App\Http\Controllers\Admin;

use App\Models\adminlog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $page_title = 'Activity Log';
        $empty_message = "Activity Log Not Found!";
        $items = adminlog::orderBy('id','desc')
        ->paginate(getPaginate());
        return view('admin.activity',compact('page_title','items','empty_message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\adminlog  $adminlog
     * @return \Illuminate\Http\Response
     */
    public function show(adminlog $adminlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\adminlog  $adminlog
     * @return \Illuminate\Http\Response
     */
    public function edit(adminlog $adminlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\adminlog  $adminlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, adminlog $adminlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\adminlog  $adminlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(adminlog $adminlog)
    {
        //
    }
}
