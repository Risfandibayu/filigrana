<?php

namespace App\Http\Controllers\Admin;

use App\Models\gachamasterdetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;


class GachamasterdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gachamasterdetail  $gachamasterdetail
     * @return \Illuminate\Http\Response
     */
    public function show(gachamasterdetail $gachamasterdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gachamasterdetail  $gachamasterdetail
     * @return \Illuminate\Http\Response
     */
    public function edit(gachamasterdetail $gachamasterdetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gachamasterdetail  $gachamasterdetail
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        // dd($request->all());
        $prod = new gachamasterdetail();
        $prod->gacha_id                     = $request->gacha_id;
        $prod->name                     = $request->name;
        $prod->qty           = $request->qty;
        $prod->modal           = $request->modal;
        $prod->type           = $request->type;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/gacha/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/gacha/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,' add Detail Gacha '.$prod->name);

        $notify[] = ['success', 'Gacha Detail edited successfully'];
        return back()->withNotify($notify);
    }
    public function update(Request $request)
    {
        //
        $prod = gachamasterdetail::where('id',$request->id)->first();
        $prod->name                     = $request->name;
        $prod->qty           = $request->qty;
        $prod->modal           = $request->modal;
        $prod->type           = $request->type;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/gacha/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/gacha/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,' Update Detail Gacha '.$prod->name);

        $notify[] = ['success', 'Gacha Detail edited successfully'];
        return back()->withNotify($notify);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gachamasterdetail  $gachamasterdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(gachamasterdetail $gachamasterdetail)
    {
        //
    }
}
