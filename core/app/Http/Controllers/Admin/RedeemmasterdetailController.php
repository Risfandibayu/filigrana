<?php

namespace App\Http\Controllers\Admin;

use App\Models\redeemmasterdetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class RedeemmasterdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\redeemmasterdetail  $redeemmasterdetail
     * @return \Illuminate\Http\Response
     */
    public function show(redeemmasterdetail $redeemmasterdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\redeemmasterdetail  $redeemmasterdetail
     * @return \Illuminate\Http\Response
     */
    public function edit(redeemmasterdetail $redeemmasterdetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\redeemmasterdetail  $redeemmasterdetail
     * @return \Illuminate\Http\Response
     */
    public function update2(Request $request, redeemmasterdetail $redeemmasterdetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\redeemmasterdetail  $redeemmasterdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(redeemmasterdetail $redeemmasterdetail)
    {
        //
    }

    public function add(Request $request)
    {
        // dd($request->all());
        $prod = new redeemmasterdetail();
        $prod->item_id                     = $request->item_id;
        $prod->name                     = $request->name;
        $prod->qty           = $request->qty;
        $prod->modal           = $request->modal;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/redeem/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/redeem/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,' add Detail Redeem '.$prod->name);

        $notify[] = ['success', 'Redeem Detail edited successfully'];
        return back()->withNotify($notify);
    }
    public function update(Request $request)
    {
        //
        $prod = redeemmasterdetail::where('id',$request->id)->first();
        $prod->name                     = $request->name;
        $prod->qty           = $request->qty;
        $prod->modal           = $request->modal;
        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $filename = time() . '_image_' . strtolower(str_replace(" ", "",$prod->name)) . '.jpg';
            $location = 'assets/images/redeem/' . $filename;
            $prod->image = $filename;

            $path = './assets/images/redeem/';

            $link = $path . $prod->image;
            if (file_exists($link)) {
                @unlink($link);
            }
            // $size = imagePath()['product']['size'];
            $image = Image::make($image);
            // $size = explode('x', strtolower($size));
            // $image->crop($size[0], $size[1]);
            $image->save($location);
        }
        $prod->save();

        adminlog(Auth::guard('admin')->user()->id,' Update Detail Redeem '.$prod->name);

        $notify[] = ['success', 'Redeem Detail edited successfully'];
        return back()->withNotify($notify);
    }
}
