<?php

namespace App\Http\Controllers\Admin;

use App\Models\redeemmaster;
use App\Http\Controllers\Controller;
use App\Models\redeemmasterdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedeemmasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $page_title = 'Redeem Master';
        $empty_message = 'No Redeem Data found';
        $redeem = redeemmaster::paginate(getPaginate());
        return view('admin.redeem.index', compact('page_title','redeem', 'empty_message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\redeemmaster  $redeemmaster
     * @return \Illuminate\Http\Response
     */
    public function show(redeemmaster $redeemmaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\redeemmaster  $redeemmaster
     * @return \Illuminate\Http\Response
     */
    public function edit(redeemmaster $redeemmaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\redeemmaster  $redeemmaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, redeemmaster $redeemmaster)
    {
        //
                //
                $prod = redeemmaster::where('id',$request->id)->first();
                $prod->name                     = $request->name ?? $prod->name;
                $prod->status           = $request->status?1:0;
                $prod->save();
        
                adminlog(Auth::guard('admin')->user()->id,'Update Gacha '.$prod->name);
        
                $notify[] = ['success', 'Gacha edited successfully'];
                return back()->withNotify($notify);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\redeemmaster  $redeemmaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(redeemmaster $redeemmaster)
    {
        //
    }
    public function detail($id){
        $redeemmaster = redeemmaster::where('id',$id)->first();
        $page_title = 'Redeem Detail '.$redeemmaster->name;
        $empty_message = 'No Redeem Detail Data found';
        $redeem = redeemmasterdetail::where('item_id','=',$id)->paginate(getPaginate());

        $total = redeemmasterdetail::where('item_id', '=', $id)
        ->selectRaw('SUM(modal * qty) as total')
        ->pluck('total')
        ->first();
        $total_qty = redeemmasterdetail::where('item_id', '=', $id)
        ->selectRaw('SUM(qty) as total_qty')
        ->pluck('total_qty')
        ->first();

        $status_page = $redeemmaster->status ?? 0;

        return view('admin.redeem.detail', compact('page_title','redeem','redeemmaster','status_page','total','total_qty', 'empty_message'));
    }
}
