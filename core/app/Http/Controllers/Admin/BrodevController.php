<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportData;
use App\Models\brodev;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\DB;

class BrodevController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\brodev  $brodev
     * @return \Illuminate\Http\Response
     */
    public function show(brodev $brodev)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\brodev  $brodev
     * @return \Illuminate\Http\Response
     */
    public function edit(brodev $brodev)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\brodev  $brodev
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, brodev $brodev)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\brodev  $brodev
     * @return \Illuminate\Http\Response
     */
    public function destroy(brodev $brodev)
    {
        //
    }

    public function index(Request $request){
        if (isset($request->status)) {
            if ($request->status == 'pending') {
                $status = 2;
                $sts = 'pending';
            }else{
                $status = 1;
                $sts = 'success';
            }
            // $items = brodev::where(['status'=>$status,'ship_method'=>2])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            // // $items = brodev::where(['ship_method'=>2,'status'=>$status])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            //         ->paginate(10);
            $items = brodev::select('brodevs.*',db::raw("GROUP_CONCAT(CONCAT(products.`name`,' (', brodevdetails.qty ,')')) as productlist"))->where('brodevs.status',$status)->join('brodevdetails','brodevs.id','=','brodevdetails.brodev_id')->leftjoin('products','products.id','=','brodevdetails.product')->where(['brodevs.ship_method'=>2])->orwhere('brodevs.ship_method',null)->where('brodevs.status',$status)->orderBy('brodevs.created_at','DESC')->groupby('brodevs.id')->get();
        }else{
            $items = DB::table(function ($query) {
                $query->select('brodevs.*', DB::raw("CONCAT(products.name, ' (', COUNT(products.name), ')') AS product_count"))
                    ->from('brodevs')
                    ->leftJoin('brodevdetails', 'brodevs.id', '=', 'brodevdetails.brodev_id')
                    ->leftJoin('products', 'products.id', '=', 'brodevdetails.product')
                    ->where('brodevs.ship_method', 2)
                    ->orwhere('brodevs.ship_method', null)
                    ->groupBy('brodevs.id', 'products.name');
            }, 'subquery')
            ->select('subquery.*','users.username','users.mobile','plans.name as pname', db::raw("GROUP_CONCAT(product_count) AS productlist"))
            ->join('users', 'users.id', '=', 'subquery.user_id')
            ->leftjoin('plans', 'users.plan_id', '=', 'plans.id')
            ->groupBy('subquery.id')
            ->orderBy('subquery.created_at', 'DESC')
            ->get();
            $sts = '';
        }
        $page_title = 'BRO Package Delivery';
        $empty_message = "BRO Package Delivery Request Not Found!";
       
        // dd($items);
        return view('admin.delivery.BroDelivery',compact('page_title','items','empty_message','sts'));
    }
    public function index3(Request $request){
        if (isset($request->status)) {
            if ($request->status == 'pending') {
                $status = 2;
                $sts = 'pending';
            }else{
                $status = 1;
                $sts = 'success';
            }
            // $items = brodev::where(['status'=>$status,'ship_method'=>2])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            // // $items = brodev::where(['ship_method'=>2,'status'=>$status])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            //         ->paginate(10);
            $items = brodev::select('brodevs.*',db::raw("GROUP_CONCAT(CONCAT(products.`name`,' (', brodevdetails.qty ,')')) as productlist"))->where('brodevs.status',$status)->join('brodevdetails','brodevs.id','=','brodevdetails.brodev_id')->leftjoin('products','products.id','=','brodevdetails.product')->where(['brodevs.ship_method'=>3])->where('brodevs.status',$status)->orderBy('brodevs.created_at','DESC')->groupby('brodevs.id')->get();
        }else{
            $items = DB::table(function ($query) {
                $query->select('brodevs.*', DB::raw("CONCAT(products.name, ' (', COUNT(products.name), ')') AS product_count"))
                    ->from('brodevs')
                    ->leftJoin('brodevdetails', 'brodevs.id', '=', 'brodevdetails.brodev_id')
                    ->leftJoin('products', 'products.id', '=', 'brodevdetails.product')
                    ->where('brodevs.ship_method', 3)
                    ->groupBy('brodevs.id', 'products.name');
            }, 'subquery')
            ->select('subquery.*','users.username','users.mobile','plans.name as pname', db::raw("GROUP_CONCAT(product_count) AS productlist"))
            ->join('users', 'users.id', '=', 'subquery.user_id')
            ->leftjoin('plans', 'users.plan_id', '=', 'plans.id')
            ->groupBy('subquery.id')
            ->orderBy('subquery.created_at', 'DESC')
            ->get();
            $sts = '';
        }
        $page_title = 'BRO Gold Convert';
        $empty_message = "BRO Gold Convert Request Not Found!";
       
        // dd($items);
        return view('admin.delivery.BroDeliveryDinaran',compact('page_title','items','empty_message','sts'));
    }
    public function index2(Request $request){
        
        if (isset($request->status) && isset($request->date)) {
            if ($request->status == 2) {
                $status = 2;
                $sts = 2;
            }else{
                $status = 1;
                $sts = 1;
            } 

            $date = explode('-',$request->date);

            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }

            $start = @$date[0];
            $end = @$date[1];

            $dateSearch =$request->date;

            $items = brodev::where('ship_method',1)->where('status',$status)->whereBetween('pickupdate', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->orderBy('created_at','DESC')->get();
        }elseif (isset($request->status) && !isset($request->date)) {
            if ($request->status == 2) {
                $status = 2;
                $sts = 2;
            }else{
                $status = 1;
                $sts = 1;
            } 
            $items = brodev::where('ship_method',1)->where('status',$status)->orderBy('created_at','DESC')->get();
            $dateSearch ='';
            
        }elseif (!isset($request->status) && isset($request->date) ) {

            $date = explode('-',$request->date);

            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }

            $start = @$date[0];
            $end = @$date[1];
            
            $items = brodev::where('ship_method',1)->whereBetween('pickupdate', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->orderBy('created_at','DESC')->get();
            $sts = '';
            $dateSearch =$request->date;
        }else{
            $items = DB::table(function ($query) {
                $query->select('brodevs.*', DB::raw("CONCAT(products.name, ' (', COUNT(products.name), ')') AS product_count"))
                    ->from('brodevs')
                    ->leftJoin('brodevdetails', 'brodevs.id', '=', 'brodevdetails.brodev_id')
                    ->leftJoin('products', 'products.id', '=', 'brodevdetails.product')
                    ->where('brodevs.ship_method', 1)
                    ->groupBy('brodevs.id', 'products.name');
            }, 'subquery')
            ->select('subquery.*','users.username','users.mobile','plans.name as pname', db::raw("GROUP_CONCAT(product_count) AS productlist"))
            ->join('users', 'users.id', '=', 'subquery.user_id')
            ->leftjoin('plans', 'users.plan_id', '=', 'plans.id')
            ->groupBy('subquery.id')
            ->orderBy('subquery.created_at', 'DESC')
            ->get();
            $sts = '';
            $dateSearch ='';
        }


        $page_title = 'BRO Package Pick Up';
        $empty_message = "BRO Package Pick Up Request Not Found!";
        return view('admin.delivery.BroDeliveryPick',compact('page_title','items','empty_message','sts','dateSearch'));
    }

    public function delivery(Request $request){
        // dd($request->all());
        $sg = brodev::where('id',$request->id)->first();
        $sg->no_resi = $request->no_resi;
        $sg->status = 1;
        $sg->save();

        adminlog(Auth::guard('admin')->user()->id,'Input Resi Number '. $sg->no_resi .' in BRO Pack trx : '. $sg->trx);

        $notify[] = ['success', 'BRO Package Deliver successfully'];
        return back()->withNotify($notify);
    }

    
    public function search(Request $request)
    {
        $search = $request->search;
        if (isset($search)) {
            // $items = brodev::where(['status'=>$status,'ship_method'=>2])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            // // $items = brodev::where(['ship_method'=>2,'status'=>$status])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            //         ->paginate(10);
            $items = brodev::join('users','users.id','=','brodevs.user_id')->select('brodevs.*');
            $items = $items->where('brodevs.ship_method',2)->where(function ($user) use ($search) {
            $user->where('users.username', 'like', "%$search%")
                    ->orWhere('users.no_bro', 'like', "%$search%")
                    ->orWhere('brodevs.trx', 'like', "%$search%");
            })->orwhere('brodevs.ship_method',null)->where(function ($user) use ($search) {
                $user->where('users.username', 'like', "%$search%")
                        ->orWhere('users.no_bro', 'like', "%$search%")
                        ->orWhere('brodevs.trx', 'like', "%$search%");
            });
            $items = $items->orderBy('created_at','DESC')->paginate(10);
            $sts = '';
            
        }else{
            $items = brodev::join('users','users.id','=','brodevs.user_id')->where(['ship_method'=>2])->orwhere('ship_method',null)->select('brodevs.*');
            $items = $items->orderBy('created_at','DESC')->paginate(10);
            $sts = '';
        }
        // $items = brodev::where(function ($user) use ($search) {
        //     $user->where('username', 'like', "%$search%")
        //         ->orWhere('no_bro', 'like', "%$search%")
        //         ->orWhere('mobile', 'like', "%$search%")
        //         ->orWhere('firstname', 'like', "%$search%")
        //         ->orWhere('lastname', 'like', "%$search%");
        // });

       
        $page_title = 'BRO Pack Search - ' . $search;
        $empty_message = 'No search result found';
        return view('admin.delivery.BroDelivery', compact('page_title', 'search', 'empty_message', 'items','sts'));
    }

    public function search2(Request $request)
    {
        $search = $request->search;
        if (isset($request->status)) {
            if ($request->status == 'pending') {
                $status = 2;
                $sts = 'pending';
            }else{
                $status = 1;
                $sts = 'success';
            }
            // $items = brodev::where(['status'=>$status,'ship_method'=>2])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            // // $items = brodev::where(['ship_method'=>2,'status'=>$status])->orwhere('ship_method',null)->orderBy('created_at','DESC')->limit(50)
            //         ->paginate(10);
            $items = brodev::where('brodevs.status',$status)->where('ship_method',1)->join('users','users.id','=','brodevs.user_id')->select('brodevs.*');
            $items = $items->where(function ($user) use ($search) {
            $user->where('users.username', 'like', "%$search%")
                    ->orWhere('users.no_bro', 'like', "%$search%")
                    ->orWhere('brodevs.trx', 'like', "%$search%");
            });
            $items = $items->paginate(10);
        }else{
            $items = brodev::join('users','users.id','=','brodevs.user_id')->where('ship_method',1)->select('brodevs.*');
            $items = $items->where(function ($user) use ($search) {
            $user->where('users.username', 'like', "%$search%")
                    ->orWhere('users.no_bro', 'like', "%$search%")
                    ->orWhere('brodevs.trx', 'like', "%$search%");
                    });
            $items = $items->paginate(10);
            $sts = '';
        }
        // $items = brodev::where(function ($user) use ($search) {
        //     $user->where('username', 'like', "%$search%")
        //         ->orWhere('no_bro', 'like', "%$search%")
        //         ->orWhere('mobile', 'like', "%$search%")
        //         ->orWhere('firstname', 'like', "%$search%")
        //         ->orWhere('lastname', 'like', "%$search%");
        // });

       
        $page_title = 'BRO Pack Search - ' . $search;
        $empty_message = 'No search result found';
        return view('admin.delivery.BroDeliveryPick', compact('page_title', 'search', 'empty_message', 'items','sts'));
    }

    public function export(Request $request){
        
        if (isset($request->status)) {
            if ($request->status == 'pending') {
                $status = 2;
                $sts = 'pending';
            }else{
                $status = 1;
                $sts = 'success';
            }
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
            $items = brodev::query()
            ->join('users','users.id','=','brodevs.user_id')
            ->where('brodevs.status',$status)->where(['brodevs.ship_method'=>2])
            ->orwhere('brodevs.ship_method',null)->where('brodevs.status',$status)
            ->orderBy('brodevs.created_at','DESC')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'pending','success')"))
            ;
            $search = '';
        }
        // dd($request->all());

        if (isset($request->search)) {
            $search = $request->search;
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
            $items = brodev::query()->where('brodevs.ship_method',2)
            ->join('users','users.id','=','brodevs.user_id')
            ->where(function ($user) use ($search) {
                $user->where('users.username', 'like', "%$search%")
                        ->orWhere('users.no_bro', 'like', "%$search%")
                        ->orWhere('brodevs.trx', 'like', "%$search%");
                })->orwhere('brodevs.ship_method',null)->where(function ($user) use ($search) {
                    $user->where('users.username', 'like', "%$search%")
                            ->orWhere('users.no_bro', 'like', "%$search%")
                            ->orWhere('brodevs.trx', 'like', "%$search%");
                });
                $items = $items->orderBy('brodevs.created_at','DESC')
                ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'pending','success')"))
                ;
            $sts = '';
        }

        if(!isset($request->status) && !isset($request->search)){
            $sts = '';
            $search = '';
            $items = brodev::query()->where(['brodevs.ship_method'=>2])->orwhere('brodevs.ship_method',null)->orderBy('brodevs.created_at','DESC')
            ->join('users','users.id','=','brodevs.user_id')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'pending','success')"))
            ;
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
        }

        return Excel::download(
            new ExportData($items), 'BROdelivery.xlsx');
    }
    public function export2(Request $request){
        
        if (isset($request->status) && isset($request->date) && !isset($request->search)) {
            if ($request->status == 2) {
                $status = 2;
                $sts = 2;
            }else{
                $status = 1;
                $sts = 1;
            } 

            $date = explode('-',$request->date);

            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }

            $start = @$date[0];
            $end = @$date[1];

            $dateSearch =$request->date;

            $items = brodev::query()->where('brodevs.ship_method',1)
            ->where('brodevs.status',$status)->whereBetween('brodevs.pickupdate', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])->orderBy('brodevs.created_at','DESC')
            ->join('users','users.id','=','brodevs.user_id')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'Waiting for pick up','Complete. Already Pick Up')"))
            ;
        }elseif (isset($request->status) && !isset($request->date) && !isset($request->search)) {
            if ($request->status == 2) {
                $status = 2;
                $sts = 2;
            }else{
                $status = 1;
                $sts = 1;
            } 
            $items = brodev::query()->where('brodevs.ship_method',1)->where('brodevs.status',$status)
            ->join('users','users.id','=','brodevs.user_id')
            ->orderBy('brodevs.created_at','DESC')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'Waiting for pick up','Complete. Already Pick Up')"));
            $dateSearch ='';
            
        }elseif (!isset($request->status) && isset($request->date) && !isset($request->search)) {

            $date = explode('-',$request->date);

            if(!(@strtotime($date[0]) && @strtotime($date[1]))){
                $notify[]=['error','Please provide valid date'];
                return back()->withNotify($notify);
            }

            $start = @$date[0];
            $end = @$date[1];
            
            $items = brodev::query()->where('brodevs.ship_method',1)->whereBetween('brodevs.pickupdate', [Carbon::parse($start), Carbon::parse($end)->addDays(1)])
            ->join('users','users.id','=','brodevs.user_id')
            ->orderBy('brodevs.created_at','DESC')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'Waiting for pick up','Complete. Already Pick Up')"));
            $sts = '';
            $dateSearch =$request->date;
        }
        elseif (!isset($request->status) && !isset($request->date) && isset($request->search)) {
            // dd($request->all());
            $search = $request->search;
            
            $items = brodev::query()->where('brodevs.ship_method',1)->join('users','users.id','=','brodevs.user_id')->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'Waiting for pick up','Complete. Already Pick Up')"))->where(function ($user) use ($search) {
            $user->where('users.username', 'like', "%$search%")
                    ->orWhere('users.no_bro', 'like', "%$search%")
                    ->orWhere('brodevs.trx', 'like', "%$search%");
            });
            $sts = '';
            $dateSearch ='';
        }
        else{
            $items = brodev::query()->where('brodevs.ship_method',1)
            ->join('users','users.id','=','brodevs.user_id')
            ->orderBy('brodevs.created_at','DESC')
            ->select('brodevs.created_at','brodevs.trx','users.username',db::raw("CONCAT('+',' ',users.mobile) AS no_hp"),'brodevs.bro_qty',db::raw("if(brodevs.status = 2, 'Waiting for pick up','Complete. Already Pick Up')"));
            $sts = '';
            $dateSearch ='';
        }


        return Excel::download(
            new ExportData($items), 'BROpickup.xlsx');
    }

    public function resendEmail($user_id, $brodevtrx){
        sendEmail2($user_id, 'select_product', [
            'link' => route('user.plan.select.type',[$user_id,$brodevtrx]),
        ]);

        $notify[] = ['success', 'Resend Email successfully'];
        return back()->withNotify($notify);
    }

    public function acceptDinaran($user_id, $brodevtrx){
        $sg = brodev::where('trx',$brodevtrx)->first();
        $sg->status = 1;
        $sg->save();

        $notify[] = ['success', 'Accept Successfully'];
        return back()->withNotify($notify);
    }
    public function rejectDinaran($user_id, $brodevtrx){
        $sg = brodev::where('trx',$brodevtrx)->first();
        $sg->status = 3;
        $sg->save();

        $notify[] = ['success', 'Reject Successfully'];
        return back()->withNotify($notify);
    }
}
