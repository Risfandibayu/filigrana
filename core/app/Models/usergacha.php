<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usergacha extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'gachamasterdetail_id'];
    public function gachamasterdetail()
    {
        return $this->belongsTo(Gachamasterdetail::class);
    }
}
