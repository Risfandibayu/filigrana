<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class userredeem extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'item_id'];
    public function redeemmasterdetail()
    {
        return $this->belongsTo(redeemmasterdetail::class);
    }
}
